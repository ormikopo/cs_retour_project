package com.example.ormikopo.cs_retour_app.features.makereview.domain;

public interface MakeReviewInteractor {

    void saveReview(String reviewText, int reviewValue, String tourPackageId);

    interface OnMakeReviewFinishListener{

        void OnSuccess();

        void OnError();

    }

}