package com.example.ormikopo.cs_retour_app.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class Utilities {

    public static void resetHardDataReloadFlag(Fragment frg) {

        Bundle args = (frg.getArguments() != null) ? frg.getArguments() : new Bundle();

        args.putBoolean(Constants.BundleKeys.HARD_RELOAD, false);

        frg.setArguments(args);

    }

}