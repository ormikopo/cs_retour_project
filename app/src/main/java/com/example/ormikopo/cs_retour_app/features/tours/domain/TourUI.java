package com.example.ormikopo.cs_retour_app.features.tours.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class TourUI implements Parcelable {

    private String photoUrl;
    private String id;
    private String name;
    private String description;
    private int price;
    private String duration;
    private int descTextSize;

    public TourUI(String id, String name, String description, int price, String duration) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.duration = duration;
    }

    public TourUI(String photoUrl, String id, String name, String description, int price, String duration) {
        this.photoUrl = photoUrl;
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDescTextSize() {
        return descTextSize;
    }

    public void setDescTextSize(int descTextSize) {
        this.descTextSize = descTextSize;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    protected TourUI(Parcel in) {
        photoUrl = in.readString();
        id = in.readString();
        name = in.readString();
        description = in.readString();
        descTextSize = in.readInt();
        price = in.readInt();
        duration = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photoUrl);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(descTextSize);
        dest.writeInt(price);
        dest.writeString(duration);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TourUI> CREATOR = new Parcelable.Creator<TourUI>() {
        @Override
        public TourUI createFromParcel(Parcel in) {
            return new TourUI(in);
        }

        @Override
        public TourUI[] newArray(int size) {
            return new TourUI[size];
        }
    };

}