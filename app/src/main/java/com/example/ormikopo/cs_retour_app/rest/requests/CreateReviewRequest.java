package com.example.ormikopo.cs_retour_app.rest.requests;

public class CreateReviewRequest {

    private int score;
    private String comment;
    private String username;

    public CreateReviewRequest(int score, String comment, String username) {
        this.score = score;
        this.comment = comment;
        this.username = username;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}