package com.example.ormikopo.cs_retour_app.features.reviews.data;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewDomain;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsInteractor;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsRepository;

import java.util.ArrayList;

public class ReviewsInteractorImpl implements ReviewsInteractor, ReviewsRepository.OnGetReviewsListener {

    private ReviewsInteractor.OnReviewsFinishListener reviewsListener;
    private ReviewsRepository reviewsRepository;

    public ReviewsInteractorImpl(ReviewsInteractor.OnReviewsFinishListener reviewsListener, Context context, boolean hardReloadDataFromNetwork) {
        this.reviewsListener = reviewsListener;
        this.reviewsRepository = new ReviewsRepositoryImpl(context, hardReloadDataFromNetwork);
    }

    @Override
    public void getReviews(String tourPackageId) {
        this.reviewsRepository.getReviews(this, tourPackageId);
    }

    @Override
    public void getFilteredReviews(String tourPackageId, String filter) {
        this.reviewsRepository.getFilteredReviews(this, tourPackageId, filter);
    }

    @Override
    public void OnGetReviewsSuccess(ArrayList<ReviewEntity> reviews) {

        ArrayList<ReviewDomain> result = new ArrayList<>();

        for (ReviewEntity review :  reviews)
        {
            result.add(
                    new ReviewDomain(
                            review.getUsername(),
                            review.getScore(),
                            review.getComment()
                    )
            );
        }

        this.reviewsListener.OnSuccess(result);

    }

    @Override
    public void OnGetReviewsError() {
        this.reviewsListener.OnError();
    }

}