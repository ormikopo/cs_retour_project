package com.example.ormikopo.cs_retour_app.features.tourpackages.data;

import android.content.Context;
import android.os.AsyncTask;

import com.example.ormikopo.cs_retour_app.base.RetourDatabase;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageRegion;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesRepository;
import com.example.ormikopo.cs_retour_app.rest.RestClient;
import com.example.ormikopo.cs_retour_app.rest.TourPackageService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class TourPackagesRepositoryImpl implements TourPackagesRepository {

    private TourPackageDAO tourPackageDAO;
    private TourPackageService tourPackageService;
    private boolean hardReloadDataFromNetwork;

    public TourPackagesRepositoryImpl(Context context, boolean hardReloadDataFromNetwork) {
        this.tourPackageDAO = RetourDatabase.getDatabase(context).tourPackageDAO();
        this.tourPackageService = RestClient.getInstance().create(TourPackageService.class);
        this.hardReloadDataFromNetwork = hardReloadDataFromNetwork;
    }

    @Override
    public void getTourPackages(final OnGetTourPackagesListener listener) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                final Random random = new Random();

                // First fetch from the DB
                final List<TourPackageEntity> tourPackagesFromDb = tourPackageDAO.getAll();

                if(tourPackagesFromDb != null && tourPackagesFromDb.size() > 0 && !hardReloadDataFromNetwork) {

                    ArrayList<TourPackageEntity> result = new ArrayList<>();

                    for (TourPackageEntity tourPackageFromDb : tourPackagesFromDb) {
                        result.add(tourPackageFromDb);
                    }

                    listener.OnGetTourPackagesSuccess(result);

                }
                // If nothing in DB or user has clicked refresh button then go to network
                else {

                    Call<ArrayList<TourPackageEntity>> call = tourPackageService.fetchAvailableTourPackages();

                    call.enqueue(new Callback<ArrayList<TourPackageEntity>>() {

                        @Override
                        public void onResponse(Call<ArrayList<TourPackageEntity>> call, final Response<ArrayList<TourPackageEntity>> response) {
                            try {

                                // Save in the DB for caching
                                AsyncTask.execute(new Runnable() {

                                    @Override
                                    public void run() {

                                        ArrayList<TourPackageEntity> tourPackagesFromNetwork = response.body();

                                        for (TourPackageEntity tourPackageEntity : tourPackagesFromNetwork) {

                                            // Get Random Images from picsum
                                            String randomPhotoUrl = "https://picsum.photos/1000/400/?image=" + random.nextInt((100 - 1) + 1) + 1;

                                            tourPackageEntity.setPhotoUrl(randomPhotoUrl);

                                        }

                                        tourPackageDAO.updateTourPackages(tourPackagesFromNetwork.toArray(new TourPackageEntity[tourPackagesFromNetwork.size()]));

                                        listener.OnGetTourPackagesSuccess(tourPackagesFromNetwork);

                                    }

                                });


                            }
                            catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<ArrayList<TourPackageEntity>> call, Throwable t) {
                            Timber.e("Failed to fetch tour packages from the server.");
                            listener.OnGetTourPackagesError();
                        }

                    });

                }

            }

        });

    }

    @Override
    public void getFilteredTourPackages(final TourPackageRegion regionEnum, final OnGetTourPackagesListener listener) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                try {

                    // Fetch all tour packages from the DB in order to filter
                    final List<TourPackageEntity> filteredTourPackagesFromDb = tourPackageDAO.findByRegion(regionEnum);

                    ArrayList<TourPackageEntity> filteredTourPackages = new ArrayList<>();

                    if(filteredTourPackagesFromDb != null && filteredTourPackagesFromDb.size() > 0) {

                        for (TourPackageEntity tourPackageFromDb : filteredTourPackagesFromDb) {
                            filteredTourPackages.add(tourPackageFromDb);
                        }

                    }

                    listener.OnGetTourPackagesSuccess(filteredTourPackages);

                }
                catch (Exception ex) {
                    listener.OnGetTourPackagesError();
                }

            }

        });

    }
}