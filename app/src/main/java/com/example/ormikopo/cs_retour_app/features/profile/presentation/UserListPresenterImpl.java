package com.example.ormikopo.cs_retour_app.features.profile.presentation;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.profile.data.UserListInteractorImpl;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserDomain;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserListInteractor;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserListPresenter;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserListView;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserUI;

import java.util.ArrayList;

public class UserListPresenterImpl implements UserListPresenter, UserListInteractor.OnUserListFinishListener {

    private UserListView userListView;
    private UserListInteractor interactor;

    public UserListPresenterImpl(UserListView userListView, Context context, boolean hardReloadDataFromNetwork) {
        this.userListView = userListView;
        this.interactor = new UserListInteractorImpl(this, context, hardReloadDataFromNetwork);
    }

    @Override
    public void getUsers() {
        this.interactor.getUsers();
    }

    @Override
    public void OnSuccess(ArrayList<UserDomain> users) {

        ArrayList<UserUI> usersUI = new ArrayList<>();

        for (UserDomain user : users) {

            UserUI userUI = new UserUI(
                    user.getId(),
                    user.getUsername(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getEmail(),
                    user.getAddress(),
                    user.getAge(),
                    user.getTeam()
            );

            String userTeam = user.getTeam();

            if (userTeam.equals("BLUE")) {
                userUI.setTeamColor(R.color.colorBlue);
            }
            else if(userTeam.equals("YELLOW")) {
                userUI.setTeamColor(R.color.colorYellow);
            }
            else if(userTeam.equals("BLACK")) {
                userUI.setTeamColor(R.color.colorBlack);
            }
            else if(userTeam.equals("RED")) {
                userUI.setTeamColor(R.color.colorRed);
            }
            else {
                userUI.setTeamColor(R.color.colorPrimary);
            }

            usersUI.add(userUI);

        }

        this.userListView.showUsers(usersUI);

    }

    @Override
    public void OnError() {
        this.userListView.showGeneralError();
    }

}