package com.example.ormikopo.cs_retour_app.features.login.domain;

public interface LoginPresenter {

    void getSavedCredentials();

    void validateCredentials(String username, String password, boolean rememberCredentials);

}