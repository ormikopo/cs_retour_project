package com.example.ormikopo.cs_retour_app.features.tours.data;

import android.content.Context;
import android.os.AsyncTask;

import com.example.ormikopo.cs_retour_app.base.RetourDatabase;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursRepository;
import com.example.ormikopo.cs_retour_app.rest.RestClient;
import com.example.ormikopo.cs_retour_app.rest.TourService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ToursRepositoryImpl implements ToursRepository {

    private TourDAO tourDAO;
    private TourService tourService;
    private boolean hardReloadDataFromNetwork;

    public ToursRepositoryImpl(Context context, boolean hardReloadDataFromNetwork) {
        this.tourDAO = RetourDatabase.getDatabase(context).tourDAO();
        this.tourService = RestClient.getInstance().create(TourService.class);
        this.hardReloadDataFromNetwork = hardReloadDataFromNetwork;
    }

    @Override
    public void getTours(final OnGetToursListener listener, final String tourPackageId) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                final Random random = new Random();

                // First fetch from the DB
                final List<TourEntity> toursFromDb = tourDAO.findByTourPackageId(tourPackageId);

                if(toursFromDb != null && toursFromDb.size() > 0 && !hardReloadDataFromNetwork) {

                    ArrayList<TourEntity> result = new ArrayList<>();

                    for (TourEntity tourFromDb : toursFromDb) {
                        result.add(tourFromDb);
                    }

                    listener.OnGetToursSuccess(result);

                }
                // If nothing in DB or user has clicked refresh button then go to network
                else {

                    Call<ArrayList<TourEntity>> call = tourService.fetchAvailableToursOfTourPackage(tourPackageId);

                    call.enqueue(new Callback<ArrayList<TourEntity>>() {

                        @Override
                        public void onResponse(Call<ArrayList<TourEntity>> call, final Response<ArrayList<TourEntity>> response) {
                            try {

                                // Save in the DB for caching
                                AsyncTask.execute(new Runnable() {

                                    @Override
                                    public void run() {

                                        ArrayList<TourEntity> toursFromNetwork = response.body();

                                        for (TourEntity tourEntity : toursFromNetwork) {

                                            // Set the foreign key TourPackageId before saving it to the db in order to associate the tours
                                            // with the correct tour package.
                                            tourEntity.setTourPackageId(tourPackageId);

                                            // Get Random Images from picsum
                                            String randomPhotoUrl = "https://picsum.photos/1000/400/?image=" + random.nextInt((100 - 1) + 1) + 1;

                                            tourEntity.setPhotoUrl(randomPhotoUrl);

                                        }

                                        tourDAO.updateToursOfTourPackage(tourPackageId, toursFromNetwork.toArray(new TourEntity[toursFromNetwork.size()]));

                                        listener.OnGetToursSuccess(toursFromNetwork);

                                    }

                                });


                            }
                            catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<ArrayList<TourEntity>> call, Throwable t) {
                            Timber.e("Failed to fetch tours from the server.");
                            listener.OnGetToursError();
                        }

                    });

                }

            }

        });

    }

}