package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

import java.util.ArrayList;

public interface TourPackagesInteractor {

    void getTourPackages();

    void getFilteredTourPackages(TourPackageRegion filterByRegion);

    interface OnTourPackagesFinishListener {

        void OnSuccess(ArrayList<TourPackageDomain> tourPackages);

        void OnError();

    }

}