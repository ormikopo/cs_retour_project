package com.example.ormikopo.cs_retour_app.rest;

import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackageEntity;
import com.example.ormikopo.cs_retour_app.utils.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TourPackageService {

    @GET(Constants.RestApiEndpoints.GET_TOUR_PACKAGES)
    Call<ArrayList<TourPackageEntity>> fetchAvailableTourPackages();

}