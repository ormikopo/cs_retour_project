package com.example.ormikopo.cs_retour_app.features.tourpackages.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.example.ormikopo.cs_retour_app.base.GenericDAO;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageRegion;

import java.util.List;

@Dao
public abstract class TourPackageDAO extends GenericDAO<TourPackageEntity> {

    @Query("SELECT * FROM tour_packages")
    public abstract List<TourPackageEntity> getAll();

    @Query("SELECT * FROM tour_packages WHERE id = :tourPackageId")
    public abstract TourPackageEntity getById(String tourPackageId);

    @Query("SELECT * FROM tour_packages WHERE region = :region")
    public abstract List<TourPackageEntity> findByRegion(TourPackageRegion region);

    @Transaction
    public void updateTourPackages(TourPackageEntity... tourPackageEntities) {
        deleteAll();
        insertMany(tourPackageEntities);
    }

    @Transaction
    public void updateTourPackageAverageRating(String tourPackageId, float tourPackageAverageRating) {

        TourPackageEntity tourPackageToUpdate = getById(tourPackageId);

        if(tourPackageToUpdate != null) {

            tourPackageToUpdate.setTotalRating(tourPackageAverageRating);

            update(tourPackageToUpdate);

        }

    }

    @Query("DELETE FROM tour_packages")
    public abstract void deleteAll();

}