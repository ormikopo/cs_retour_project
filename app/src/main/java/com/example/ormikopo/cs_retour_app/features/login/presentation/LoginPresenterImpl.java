package com.example.ormikopo.cs_retour_app.features.login.presentation;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.login.data.LoginInteractorImpl;
import com.example.ormikopo.cs_retour_app.features.login.domain.LoginInteractor;
import com.example.ormikopo.cs_retour_app.features.login.domain.LoginPresenter;
import com.example.ormikopo.cs_retour_app.features.login.domain.LoginView;

public class LoginPresenterImpl implements
        LoginPresenter,
        LoginInteractor.OnGetSavedCredentialsFinishListener,
        LoginInteractor.OnValidateCredentialsFinishListener {

    private LoginView loginView;
    private LoginInteractor interactor;

    public LoginPresenterImpl(LoginView loginView, Context context) {
        this.loginView = loginView;
        this.interactor = new LoginInteractorImpl(context, this, this);
    }

    @Override
    public void getSavedCredentials() {
        this.interactor.getSavedCredentials();
    }

    @Override
    public void validateCredentials(String username, String password, boolean rememberCredentials) {
        this.interactor.validateCredentials(username, password, rememberCredentials);
    }

    @Override
    public void OnGetSavedCredentialsSuccess(String username, String password) {
        this.loginView.fillSavedCredentials(username, password);
    }

    @Override
    public void OnGetSavedCredentialsError() {
        this.loginView.showGeneralError();
    }

    @Override
    public void OnValidateCredentialsSuccess(boolean validationResult) {

        if(validationResult) {
            this.loginView.navigateToMainActivity();
        }
        else {
            this.loginView.showInvalidCredentialsMessage();
        }

    }

    @Override
    public void OnValidateCredentialsError() {
        this.loginView.showGeneralError();
    }

}