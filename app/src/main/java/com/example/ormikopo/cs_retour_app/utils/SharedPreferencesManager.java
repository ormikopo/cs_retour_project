package com.example.ormikopo.cs_retour_app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.example.ormikopo.cs_retour_app.utils.PreferencesUtility.REMEMBER_CREDENTIALS;
import static com.example.ormikopo.cs_retour_app.utils.PreferencesUtility.SIGNED_IN_USER_ID;
import static com.example.ormikopo.cs_retour_app.utils.PreferencesUtility.SIGN_IN_PASSWORD;
import static com.example.ormikopo.cs_retour_app.utils.PreferencesUtility.SIGN_IN_USERNAME;

public class SharedPreferencesManager {

    static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Set the Sign In Username
     * @param context
     * @param userName
     */
    public static void setSignInUsername(Context context, String userName) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(SIGN_IN_USERNAME, userName);
        editor.apply();
    }

    /**
     * Get the Sign In Username
     * @param context
     * @return String: sign in username
     */
    public static String getSignInUsername(Context context) {
        return getPreferences(context).getString(SIGN_IN_USERNAME, null);
    }

    /**
     * Set the Sign In Password
     * @param context
     * @param password
     */
    public static void setSignInPassword(Context context, String password) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(SIGN_IN_PASSWORD, password);
        editor.apply();
    }

    /**
     * Get the Sign In Password
     * @param context
     * @return String: sign in password
     */
    public static String getSignInPassword(Context context) {
        return getPreferences(context).getString(SIGN_IN_PASSWORD, null);
    }

    /**
     * Set the Signed In User Id
     * @param context
     * @param userId
     */
    public static void setSignedInUserId(Context context, int userId) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putInt(SIGNED_IN_USER_ID, userId);
        editor.apply();
    }

    /**
     * Get the Signed In User Id
     * @param context
     * @return int: signed in user id
     */
    public static int getSignedInUserId(Context context) {
        return getPreferences(context).getInt(SIGNED_IN_USER_ID, -1);
    }

    /**
     * Set the Remember Credentials Preference
     * @param context
     * @param rememberCredentials
     */
    public static void setRememberCredentialsPreference(Context context, boolean rememberCredentials) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(REMEMBER_CREDENTIALS, rememberCredentials);
        editor.apply();
    }

    /**
     * Get the Remember Credentials Preference
     * @param context
     * @return boolean: user remember credentials preference
     */
    public static boolean getRememberCredentialsPreference(Context context) {
        return getPreferences(context).getBoolean(REMEMBER_CREDENTIALS, false);
    }

}