package com.example.ormikopo.cs_retour_app.features.tourpackages.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.OnTourPackageClickListener;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TourPackagesRvAdapter extends RecyclerView.Adapter<TourPackagesRvAdapter.TourPackagesViewHolder> {

    private ArrayList<TourPackageUI> tourPackages;
    private OnTourPackageClickListener listener;
    private Context context;

    public TourPackagesRvAdapter(ArrayList<TourPackageUI> tourPackages, OnTourPackageClickListener listener, Context context) {
        this.tourPackages = tourPackages;
        this.listener = listener;
        this.context = context;
    }

    public static class TourPackagesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tour_package_logo)
        ImageView mTourPackageLogo;

        @BindView(R.id.tour_package_name)
        TextView mTourPackageName;

        @BindView(R.id.tour_package_region)
        TextView mTourPackageRegion;

        @BindView(R.id.tour_package_rating)
        RatingBar mTourPackageRating;

        @BindView(R.id.rating_text_value)
        TextView mRatingTextValue;

        @BindView(R.id.tour_package_rate_button)
        Button mTourPackageRateButton;

        @BindView(R.id.tour_package_item_root)
        LinearLayout mTourPackageItemRoot;

        public TourPackagesViewHolder(View v) {

            super(v);

            ButterKnife.bind(this, v);

        }
    }

    @NonNull
    @Override
    public TourPackagesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater
                    .from(viewGroup.getContext())
                    .inflate(R.layout.view_tour_package_item, viewGroup, false);

        TourPackagesViewHolder vh = new TourPackagesViewHolder(view);

        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull TourPackagesViewHolder holder, int position) {

        final TourPackageUI tourPackage = this.tourPackages.get(position);

        Picasso.get()
            .load(tourPackage.getPhotoUrl())
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(holder.mTourPackageLogo);

        holder.mTourPackageName.setText(tourPackage.getName());

        holder.mTourPackageRegion.setText(tourPackage.getRegion().getRegionStringValue());
        holder.mTourPackageRegion.setTextColor(context.getResources().getColor(tourPackage.getRegionColor()));

        holder.mTourPackageRating.setRating(tourPackage.getRating());

        float tourPackageRating = tourPackage.getRating();

        if(tourPackageRating == 0.0) {
            holder.mRatingTextValue.setText(null);
        }
        else {
            holder.mRatingTextValue.setText("(" + String.valueOf(tourPackageRating) + ")");
        }

        holder.mRatingTextValue.setTextColor(context.getResources().getColor(tourPackage.getRatingColor()));

        holder.mTourPackageItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTourPackageClicked(tourPackage);
            }
        });

        holder.mTourPackageRateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnTourPackageRateButtonClicked(tourPackage);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.tourPackages.size();
    }

}