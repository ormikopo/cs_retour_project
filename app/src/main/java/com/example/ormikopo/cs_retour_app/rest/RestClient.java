package com.example.ormikopo.cs_retour_app.rest;

import com.example.ormikopo.cs_retour_app.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static Retrofit retrofit;

    static {
        setupRestClient();
    }

    public static Retrofit getInstance() {
        return retrofit;
    }

    private static void setupRestClient() {

        OkHttpClient mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(Constants.RestClientSettings.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.RestClientSettings.READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.RestApiEndpoints.BASE_URL)
                .client(mOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

}