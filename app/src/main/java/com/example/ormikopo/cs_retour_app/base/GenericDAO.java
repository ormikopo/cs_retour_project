package com.example.ormikopo.cs_retour_app.base;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

public abstract class GenericDAO<TEntity> {

    /**
     * Insert an object in the database.
     *
     * @param entity the object to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(TEntity entity);

    /**
     * Insert an array of objects in the database.
     *
     * @param entities the objects to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertMany(TEntity... entities);

    /**
     * Update an object from the database.
     *
     * @param entity the object to be updated
     */
    @Update
    public abstract void update(TEntity entity);

    /**
     * Delete an object from the database
     *
     * @param entity the object to be deleted
     */
    @Delete
    public abstract void delete(TEntity entity);

}