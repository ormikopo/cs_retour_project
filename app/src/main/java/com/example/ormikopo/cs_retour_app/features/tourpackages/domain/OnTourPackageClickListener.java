package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

public interface OnTourPackageClickListener {

    void onTourPackageClicked(TourPackageUI tourPackage);

    void OnTourPackageRateButtonClicked(TourPackageUI tourPackage);

}