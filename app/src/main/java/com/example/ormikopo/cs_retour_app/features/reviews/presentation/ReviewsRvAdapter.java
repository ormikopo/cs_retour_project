package com.example.ormikopo.cs_retour_app.features.reviews.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewUI;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewsRvAdapter extends RecyclerView.Adapter<ReviewsRvAdapter.ReviewsViewHolder> {

    private ArrayList<ReviewUI> reviews;
    private Context context;

    public ReviewsRvAdapter(ArrayList<ReviewUI> reviews, Context context) {
        this.reviews = reviews;
        this.context = context;
    }

    public static class ReviewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.reviewer_name)
        TextView mReviewerName;

        @BindView(R.id.reviewer_rating)
        RatingBar mReviewRating;

        @BindView(R.id.rating_text_value)
        TextView mRatingTextValue;

        @BindView(R.id.reviewer_comment)
        TextView mReviewDescription;

        public ReviewsViewHolder(View v) {

            super(v);

            ButterKnife.bind(this, v);

        }
    }

    @NonNull
    @Override
    public ReviewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.view_review_item, viewGroup, false);

        ReviewsViewHolder vh = new ReviewsViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsViewHolder holder, int position) {

        final ReviewUI review = this.reviews.get(position);

        holder.mReviewerName.setText(review.getReviewerName());
        holder.mReviewDescription.setText(review.getDescription());
        holder.mReviewRating.setRating(review.getRating());

        holder.mRatingTextValue.setText("(" + String.valueOf(review.getRating()) + ")");
        holder.mRatingTextValue.setTextColor(context.getResources().getColor(review.getRatingColor()));

    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

}