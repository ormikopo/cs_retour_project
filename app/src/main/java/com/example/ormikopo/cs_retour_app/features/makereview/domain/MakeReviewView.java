package com.example.ormikopo.cs_retour_app.features.makereview.domain;

public interface MakeReviewView {

    void showSuccessMessage();

    void showGeneralError();

}