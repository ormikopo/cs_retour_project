package com.example.ormikopo.cs_retour_app.utils;

public class PreferencesUtility {

    // Values for Shared Preferences
    public static final String SIGN_IN_USERNAME = "sign_in_username";

    public static final String SIGN_IN_PASSWORD = "sign_in_password";

    public static final String SIGNED_IN_USER_ID = "signed_in_user_id";

    public static final String REMEMBER_CREDENTIALS = "remember_credentials";

}