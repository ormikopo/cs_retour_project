package com.example.ormikopo.cs_retour_app.features.login.data;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.login.domain.LoginInteractor;
import com.example.ormikopo.cs_retour_app.features.profile.data.UserRepositoryImpl;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserRepository;

public class LoginInteractorImpl implements
        LoginInteractor,
        UserRepository.OnGetSavedCredentialsListener,
        UserRepository.OnValidateCredentialsListener {

    private LoginInteractor.OnGetSavedCredentialsFinishListener getSavedCredentialsListener;
    private LoginInteractor.OnValidateCredentialsFinishListener validateCredentialsListener;
    private UserRepository userRepository;

    public LoginInteractorImpl(Context context, LoginInteractor.OnGetSavedCredentialsFinishListener getSavedCredentialsListener, LoginInteractor.OnValidateCredentialsFinishListener validateCredentialsListener) {
        this.getSavedCredentialsListener = getSavedCredentialsListener;
        this.validateCredentialsListener = validateCredentialsListener;
        this.userRepository = new UserRepositoryImpl(context, false);
    }

    @Override
    public void getSavedCredentials() {
        this.userRepository.getSavedCredentials(this);
    }

    @Override
    public void validateCredentials(String username, String password, boolean rememberCredentials) {
        this.userRepository.validateCredentials(username, password, rememberCredentials, this);
    }

    @Override
    public void OnGetSavedUserCredentialsSuccess(String username, String password) {
        this.getSavedCredentialsListener.OnGetSavedCredentialsSuccess(username, password);
    }

    @Override
    public void OnGetSavedUserCredentialsError() {
        this.getSavedCredentialsListener.OnGetSavedCredentialsError();
    }

    @Override
    public void OnValidateSuccess(boolean validationResult) {
        this.validateCredentialsListener.OnValidateCredentialsSuccess(validationResult);
    }

    @Override
    public void OnValidateError() {
        this.validateCredentialsListener.OnValidateCredentialsError();
    }

}