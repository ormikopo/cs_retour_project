package com.example.ormikopo.cs_retour_app.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.example.ormikopo.cs_retour_app.R;

public class FragmentContainerHelper {

    public static <TFragment extends Fragment> void replaceActivityMainContentWithFragment(
            FragmentActivity currentActivity,
            TFragment replacementFragment,
            String fragmentTag
    ) {
        commitFragmentTransaction(currentActivity, replacementFragment, fragmentTag);
    }

    private static <TFragment extends Fragment> void commitFragmentTransaction(
            FragmentActivity currentActivity,
            TFragment replacementFragment,
            String fragmentTag
    ) {

        FragmentTransaction transaction = currentActivity.getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the main_content container with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        // The addToBackStack() method takes an optional string parameter that specifies a unique name for the transaction.
        // The name isn't needed unless you plan to perform advanced fragment operations using the FragmentManager.BackStackEntry APIs.
        transaction.replace(R.id.main_content, replacementFragment, fragmentTag);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }

}