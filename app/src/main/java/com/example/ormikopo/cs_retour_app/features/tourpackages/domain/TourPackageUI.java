package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class TourPackageUI implements Parcelable {

    private String photoUrl;
    private String id;
    private String name;
    private float rating;
    private int ratingColor;
    private int regionColor;
    private TourPackageRegion region;

    public TourPackageUI(String photoUrl, String id, String name, float rating, TourPackageRegion region) {

        this.photoUrl = photoUrl;
        this.id = id;
        this.name = name;
        this.rating = rating;
        this.region = region;
    }

    public TourPackageUI(String id, String name, float rating, TourPackageRegion region) {
        this.id = id;
        this.name = name;
        this.rating = rating;
        this.region = region;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getRatingColor() {
        return ratingColor;
    }

    public void setRatingColor(int ratingColor) {
        this.ratingColor = ratingColor;
    }

    public int getRegionColor() {
        return regionColor;
    }

    public void setRegionColor(int regionColor) {
        this.regionColor = regionColor;
    }

    public TourPackageRegion getRegion() {
        return region;
    }

    public void setRegion(TourPackageRegion region) {
        this.region = region;
    }

    protected TourPackageUI(Parcel in) {
        photoUrl = in.readString();
        id = in.readString();
        name = in.readString();
        rating = in.readFloat();
        ratingColor = in.readInt();
        regionColor = in.readInt();
        region = (TourPackageRegion) in.readValue(TourPackageRegion.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photoUrl);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeFloat(rating);
        dest.writeInt(ratingColor);
        dest.writeInt(regionColor);
        dest.writeValue(region);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TourPackageUI> CREATOR = new Parcelable.Creator<TourPackageUI>() {
        @Override
        public TourPackageUI createFromParcel(Parcel in) {
            return new TourPackageUI(in);
        }

        @Override
        public TourPackageUI[] newArray(int size) {
            return new TourPackageUI[size];
        }
    };

}