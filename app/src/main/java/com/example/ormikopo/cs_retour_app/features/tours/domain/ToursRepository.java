package com.example.ormikopo.cs_retour_app.features.tours.domain;

import com.example.ormikopo.cs_retour_app.features.tours.data.TourEntity;

import java.util.ArrayList;

public interface ToursRepository {

    void getTours(OnGetToursListener listener, String tourPackageId);

    interface OnGetToursListener {

        void OnGetToursSuccess(ArrayList<TourEntity> tours);

        void OnGetToursError();

    }

}