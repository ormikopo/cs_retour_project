package com.example.ormikopo.cs_retour_app.base;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.ormikopo.cs_retour_app.features.profile.data.UserDAO;
import com.example.ormikopo.cs_retour_app.features.profile.data.UserEntity;
import com.example.ormikopo.cs_retour_app.features.reviews.data.ReviewDAO;
import com.example.ormikopo.cs_retour_app.features.reviews.data.ReviewEntity;
import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackageDAO;
import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackageEntity;
import com.example.ormikopo.cs_retour_app.features.tours.data.TourDAO;
import com.example.ormikopo.cs_retour_app.features.tours.data.TourEntity;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.CustomTypeConverter;

@Database(entities = {TourPackageEntity.class, TourEntity.class, ReviewEntity.class, UserEntity.class}, version = 1, exportSchema = false)
@TypeConverters(CustomTypeConverter.class)
public abstract class RetourDatabase extends RoomDatabase {

    public abstract TourPackageDAO tourPackageDAO();

    public abstract TourDAO tourDAO();

    public abstract ReviewDAO reviewDAO();

    public abstract UserDAO userDAO();

    private static volatile RetourDatabase INSTANCE;

    public static RetourDatabase getDatabase(final Context context) {

        if (INSTANCE == null) {

            synchronized (RetourDatabase.class) {

                if (INSTANCE == null) {

                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RetourDatabase.class, Constants.DbConfig.DATABASE_NAME)
                            .addCallback(sRoomDatabaseCallback) // for initial seeding of the database
                            .build();

                }

            }

        }

        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }

            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final TourPackageDAO mTourPackageDao;
        private final TourDAO mTourDao;
        private final ReviewDAO mReviewDao;
        private final UserDAO mUserDao;

        PopulateDbAsync(RetourDatabase db) {
            this.mTourPackageDao = db.tourPackageDAO();
            this.mTourDao = db.tourDAO();
            this.mReviewDao = db.reviewDAO();
            this.mUserDao = db.userDAO();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            // Database seed code goes here


            return null;
        }
    }

}