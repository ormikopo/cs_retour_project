package com.example.ormikopo.cs_retour_app.features.tours.domain;

public interface OnTourClickListener {

    void onTourClicked(TourUI tour);

}