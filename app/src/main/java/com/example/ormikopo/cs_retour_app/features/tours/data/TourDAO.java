package com.example.ormikopo.cs_retour_app.features.tours.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.example.ormikopo.cs_retour_app.base.GenericDAO;

import java.util.List;

@Dao
public abstract class TourDAO extends GenericDAO<TourEntity> {

    @Query("SELECT * FROM tours")
    public abstract List<TourEntity> getAll();

    @Query("SELECT * FROM tours WHERE id = :id")
    public abstract TourEntity findById(int id);

    @Query("SELECT * FROM tours WHERE tour_package_id = :tourPackageId")
    public abstract List<TourEntity> findByTourPackageId(String tourPackageId);

    @Transaction
    public void updateToursOfTourPackage(String tourPackageId, TourEntity... tourEntities) {
        deleteToursOfTourPackage(tourPackageId);
        insertMany(tourEntities);
    }

    @Query("DELETE FROM tours WHERE tour_package_id = :tourPackageId")
    public abstract void deleteToursOfTourPackage(String tourPackageId);

}