package com.example.ormikopo.cs_retour_app.features.reviews.presentation;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.reviews.data.ReviewsInteractorImpl;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewDomain;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewUI;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsInteractor;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsPresenter;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsView;

import java.util.ArrayList;

public class ReviewsPresenterImpl implements ReviewsPresenter, ReviewsInteractor.OnReviewsFinishListener {

    private ReviewsView reviewsView;
    private ReviewsInteractor interactor;

    public ReviewsPresenterImpl(ReviewsView reviewsView, Context context, boolean hardReloadDataFromNetwork) {
        this.reviewsView = reviewsView;
        this.interactor = new ReviewsInteractorImpl(this, context, hardReloadDataFromNetwork);
    }

    @Override
    public void getReviews(String tourPackageId) {
        this.interactor.getReviews(tourPackageId);
    }

    @Override
    public void getFilteredReviews(String tourPackageId, String filter) {
        this.interactor.getFilteredReviews(tourPackageId, filter);
    }

    @Override
    public void OnSuccess(ArrayList<ReviewDomain> reviews) {

        ArrayList<ReviewUI> reviewsUI = new ArrayList<>();

        if (reviews != null && !reviews.isEmpty()) {

            for (ReviewDomain review : reviews) {

                ReviewUI reviewUI = new ReviewUI(
                        review.getReviewerName(),
                        review.getRating(),
                        review.getDescription()
                );

                int reviewRating = review.getRating();

                if (reviewRating > 0 && reviewRating < 3) {
                    reviewUI.setRatingColor(R.color.colorRed);
                }
                else if (reviewRating == 3) {
                    reviewUI.setRatingColor(R.color.colorYellow);
                }
                else if (reviewRating > 3 && reviewRating <= 5) {
                    reviewUI.setRatingColor(R.color.colorGreen);
                }
                else {
                    reviewUI.setRatingColor(R.color.colorPrimary);
                }

                reviewsUI.add(reviewUI);

            }

        }

        reviewsView.showReviews(reviewsUI);

    }

    @Override
    public void OnError() {
        this.reviewsView.showGeneralError();
    }
}