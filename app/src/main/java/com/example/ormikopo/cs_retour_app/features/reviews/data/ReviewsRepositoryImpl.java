package com.example.ormikopo.cs_retour_app.features.reviews.data;

import android.content.Context;
import android.os.AsyncTask;

import com.example.ormikopo.cs_retour_app.base.RetourDatabase;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsRepository;
import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackageDAO;
import com.example.ormikopo.cs_retour_app.rest.RestClient;
import com.example.ormikopo.cs_retour_app.rest.ReviewService;
import com.example.ormikopo.cs_retour_app.rest.requests.CreateReviewRequest;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ReviewsRepositoryImpl implements ReviewsRepository {

    private Context context;
    private TourPackageDAO tourPackageDAO;
    private ReviewDAO reviewDAO;
    private ReviewService reviewService;
    private boolean hardReloadDataFromNetwork;

    public ReviewsRepositoryImpl(Context context, boolean hardReloadDataFromNetwork){
        this.context = context;
        this.tourPackageDAO = RetourDatabase.getDatabase(context).tourPackageDAO();
        this.reviewDAO = RetourDatabase.getDatabase(context).reviewDAO();
        this.reviewService = RestClient.getInstance().create(ReviewService.class);
        this.hardReloadDataFromNetwork = hardReloadDataFromNetwork;
    }

    @Override
    public void getReviews(final OnGetReviewsListener listener, final String tourPackageId) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                // First fetch reviews from DB
                final List<ReviewEntity> reviewsFromDb = reviewDAO.getReviewsOfTourPackage(tourPackageId);

                if (reviewsFromDb != null && reviewsFromDb.size() > 0 && !hardReloadDataFromNetwork){

                    ArrayList<ReviewEntity> result = new ArrayList<>();

                    for (ReviewEntity reviewFromDb :  reviewsFromDb)
                    {
                        result.add(reviewFromDb);
                    }

                    listener.OnGetReviewsSuccess(result);

                }
                // If nothing in DB or user has clicked refresh button then go to network
                else {

                    Call<ArrayList<ReviewEntity>> call = reviewService.fetchTourPackageReviews(tourPackageId);

                    call.enqueue(new Callback<ArrayList<ReviewEntity>>() {

                        @Override
                        public void onResponse(Call<ArrayList<ReviewEntity>> call, final Response<ArrayList<ReviewEntity>> response) {
                            try {

                                // Save in the DB for caching
                                AsyncTask.execute(new Runnable() {

                                    @Override
                                    public void run() {

                                        ArrayList<ReviewEntity> reviewFromNetwork = response.body();

                                        int totalReviewsCount = reviewFromNetwork.size();
                                        int totalReviewsRatingSum = 0;
                                        float averageRating = 0;

                                        for (ReviewEntity tourPackageReviewEntity : reviewFromNetwork) {

                                            totalReviewsRatingSum = totalReviewsRatingSum + tourPackageReviewEntity.getScore();

                                            // Set the foreign key TourPackageId before saving it to the db in order to associate the reviews
                                            // with the correct tour package.
                                            tourPackageReviewEntity.setTourPackageId(tourPackageId);

                                        }

                                        reviewDAO.updateReviewsOfTourPackage(tourPackageId, reviewFromNetwork.toArray(new ReviewEntity[reviewFromNetwork.size()]));

                                        if(totalReviewsCount > 0) {
                                            averageRating = (float) (totalReviewsRatingSum / totalReviewsCount);
                                        }

                                        // Update Average Tour Package Rating in DB
                                        tourPackageDAO.updateTourPackageAverageRating(tourPackageId, averageRating);

                                        listener.OnGetReviewsSuccess(reviewFromNetwork);

                                    }

                                });


                            }
                            catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<ArrayList<ReviewEntity>> call, Throwable t) {
                            Timber.e("Failed to fetch tour package reviews from the server.");
                            listener.OnGetReviewsError();
                        }

                    });

                }

            }
        });

    }

    @Override
    public void getFilteredReviews(final OnGetReviewsListener listener, final String tourPackageId, final String filter) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                try {

                    // Fetch all tour package reviews from the DB in order to filter
                    final List<ReviewEntity> tourPackageReviewsFromDb = reviewDAO.getReviewsOfTourPackage(tourPackageId);

                    ArrayList<ReviewEntity> filteredReviews = new ArrayList<>();

                    if(tourPackageReviewsFromDb != null && tourPackageReviewsFromDb.size() > 0) {

                        for(int i=0; i<tourPackageReviewsFromDb.size(); i++) {

                            ReviewEntity currentReview = tourPackageReviewsFromDb.get(i);

                            String currentRatingReviewerName = currentReview.getUsername();

                            // case insensitive
                            if(currentRatingReviewerName.toLowerCase().startsWith(filter.toLowerCase())) {
                                filteredReviews.add(currentReview);
                            }

                        }

                    }

                    listener.OnGetReviewsSuccess(filteredReviews);

                }
                catch (Exception ex) {
                    listener.OnGetReviewsError();
                }

            }

        });

    }

    @Override
    public void saveReview(final OnCreateReviewListener listener, final String reviewText, final int reviewValue, final String tourPackageId) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                final String username = SharedPreferencesManager.getSignInUsername(context);

                Call<Void> call = reviewService.createTourPackageReview(tourPackageId, new CreateReviewRequest(reviewValue, reviewText, username));

                call.enqueue(new Callback<Void>() {

                    @Override
                    public void onResponse(Call<Void> call, final Response<Void> response) {
                        try {

                            // Save in the DB for caching
                            AsyncTask.execute(new Runnable() {

                                @Override
                                public void run() {

                                    if (response.code() == Constants.StatusCodes.CREATED) {

                                        reviewDAO.updateReviewOfUserForTourPackage(
                                                username,
                                                tourPackageId,
                                                new ReviewEntity(reviewValue, reviewText, username, tourPackageId)
                                        );

                                        listener.OnCreateReviewSuccess();

                                    }
                                    else {
                                        listener.OnCreateReviewError();
                                    }

                                }

                            });


                        }
                        catch (Exception e) {
                            onFailure(call, e);
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Timber.e("Failed to save tour package review to the server.");
                        listener.OnCreateReviewError();
                    }

                });

            }

        });

    }

}