package com.example.ormikopo.cs_retour_app.features.profile.domain;

public interface UserListPresenter {

    void getUsers();

}