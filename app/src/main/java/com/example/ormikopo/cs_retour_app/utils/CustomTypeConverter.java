package com.example.ormikopo.cs_retour_app.utils;

import android.arch.persistence.room.TypeConverter;

import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageRegion;

public class CustomTypeConverter {

    @TypeConverter
    public static TourPackageRegion toTourPackageRegion(String region) {

        if (region.equals(TourPackageRegion.CRETE.getRegionStringValue())) {
            return TourPackageRegion.CRETE;
        }
        else if (region.equals(TourPackageRegion.PELOPONNESE.getRegionStringValue())) {
            return TourPackageRegion.PELOPONNESE;
        }
        else if (region.equals(TourPackageRegion.MACEDONIA.getRegionStringValue())) {
            return TourPackageRegion.MACEDONIA;
        }
        else if (region.equals(TourPackageRegion.THESSALY.getRegionStringValue())) {
            return TourPackageRegion.THESSALY;
        }
        else if (region.equals(TourPackageRegion.THRACE.getRegionStringValue())) {
            return TourPackageRegion.THRACE;
        }
        else if (region.equals(TourPackageRegion.AEAGEAN.getRegionStringValue())) {
            return TourPackageRegion.AEAGEAN;
        }
        else if (region.equals(TourPackageRegion.IONIAN.getRegionStringValue())) {
            return TourPackageRegion.IONIAN;
        }
        else if (region.equals(TourPackageRegion.STEREAHELLAS.getRegionStringValue())) {
            return TourPackageRegion.STEREAHELLAS;
        }
        else {
            throw new IllegalArgumentException("Could not recognize region.");
        }

    }

    @TypeConverter
    public static String toString(TourPackageRegion region) {
        return region.getRegionStringValue();
    }

}