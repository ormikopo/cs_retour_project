package com.example.ormikopo.cs_retour_app.features.tourpackages.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageRegion;
import com.google.gson.annotations.SerializedName;

@Entity(
        tableName = "tour_packages",
        indices = {@Index("region")}
)
public class TourPackageEntity {

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "region")
    private TourPackageRegion region;

    @ColumnInfo(name = "total_rating")
    @SerializedName("averageReviewScore")
    private float totalRating;

    @ColumnInfo(name = "photo_url")
    private String photoUrl;

    public TourPackageEntity(@NonNull String id, String name, TourPackageRegion region, float totalRating, String photoUrl) {
        this.id = id;
        this.name = name;
        this.region = region;
        this.totalRating = totalRating;
        this.photoUrl = photoUrl;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TourPackageRegion getRegion() {
        return region;
    }

    public void setRegion(TourPackageRegion region) {
        this.region = region;
    }

    public float getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(float totalRating) {
        this.totalRating = totalRating;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

}