package com.example.ormikopo.cs_retour_app.features.tours.presentation;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.tours.data.ToursInteractorImpl;
import com.example.ormikopo.cs_retour_app.features.tours.domain.TourDomain;
import com.example.ormikopo.cs_retour_app.features.tours.domain.TourUI;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursInteractor;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursPresenter;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursView;

import java.util.ArrayList;

public class ToursPresenterImpl implements ToursPresenter, ToursInteractor.OnToursFinishListener {

    private ToursView toursView;
    private ToursInteractor interactor;

    public ToursPresenterImpl(ToursView toursView, Context context, boolean hardReloadDataFromNetwork) {
        this.toursView = toursView;
        this.interactor = new ToursInteractorImpl(this, context, hardReloadDataFromNetwork);
    }

    @Override
    public void getTours(String tourPackageId) {
        this.interactor.getTours(tourPackageId);
    }

    @Override
    public void OnSuccess(ArrayList<TourDomain> tours) {

        ArrayList<TourUI> toursUI = new ArrayList<>();

        if (tours != null && !tours.isEmpty()) {

            for (TourDomain tour : tours) {

                TourUI tourUI = new TourUI(
                        tour.getPhotoUrl(),
                        tour.getId(),
                        tour.getName(),
                        tour.getDescription(),
                        tour.getPrice(),
                        tour.getDuration()
                );

                if (tourUI.getDescription().length() > 30) {
                    tourUI.setDescTextSize(10);
                }
                else {
                    tourUI.setDescTextSize(15);
                }

                toursUI.add(tourUI);

            }

        }

        this.toursView.showTours(toursUI);

    }

    @Override
    public void OnError() {
        this.toursView.showGeneralError();
    }

}