package com.example.ormikopo.cs_retour_app.features.makereview.presentation;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.makereview.data.MakeReviewInteractorImpl;
import com.example.ormikopo.cs_retour_app.features.makereview.domain.MakeReviewInteractor;
import com.example.ormikopo.cs_retour_app.features.makereview.domain.MakeReviewPresenter;
import com.example.ormikopo.cs_retour_app.features.makereview.domain.MakeReviewView;

public class MakeReviewPresenterImpl implements MakeReviewPresenter, MakeReviewInteractor.OnMakeReviewFinishListener {

    private Context context;
    private MakeReviewView makeReviewView;
    private MakeReviewInteractor interactor;

    public MakeReviewPresenterImpl(MakeReviewView makeReviewView,Context context) {
        this.context = context;
        this.makeReviewView = makeReviewView;
        this.interactor = new MakeReviewInteractorImpl(this, this.context, false);
    }

    @Override
    public void saveUserReview(String reviewText, int ratingValue, String tourPackageId) {
        this.interactor.saveReview(reviewText, ratingValue, tourPackageId);
    }

    @Override
    public void OnSuccess() {
        this.makeReviewView.showSuccessMessage();
    }

    @Override
    public void OnError() {
        this.makeReviewView.showGeneralError();
    }

}