package com.example.ormikopo.cs_retour_app.features.profile.data;

import android.content.Context;
import android.os.AsyncTask;

import com.example.ormikopo.cs_retour_app.base.RetourDatabase;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserRepository;
import com.example.ormikopo.cs_retour_app.rest.RestClient;
import com.example.ormikopo.cs_retour_app.rest.UserService;
import com.example.ormikopo.cs_retour_app.rest.requests.SignOnRequest;
import com.example.ormikopo.cs_retour_app.rest.responses.SignOnResponse;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class UserRepositoryImpl implements UserRepository {

    private Context context;
    private UserDAO userDAO;
    private UserService userService;
    private boolean hardReloadDataFromNetwork;

    public UserRepositoryImpl(Context context, boolean hardReloadDataFromNetwork) {
        this.context = context;
        this.userDAO = RetourDatabase.getDatabase(context).userDAO();
        this.userService = RestClient.getInstance().create(UserService.class);
        this.hardReloadDataFromNetwork = hardReloadDataFromNetwork;
    }


    @Override
    public void getUsers(final OnGetUsersFinishListener listener) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                // First fetch from the DB
                final List<UserEntity> usersFromDb = userDAO.getAll();

                if(usersFromDb != null && usersFromDb.size() > 0 && !hardReloadDataFromNetwork) {

                    ArrayList<UserEntity> result = new ArrayList<>();

                    for (UserEntity userFromDb : usersFromDb) {
                        result.add(userFromDb);
                    }

                    listener.OnGetUsersFinishSuccess(result);

                }
                // If nothing in DB or user has clicked refresh button then go to network
                else {

                    Call<ArrayList<UserEntity>> call = userService.fetchApplicationUsers();

                    call.enqueue(new Callback<ArrayList<UserEntity>>() {

                        @Override
                        public void onResponse(Call<ArrayList<UserEntity>> call, final Response<ArrayList<UserEntity>> response) {
                            try {

                                // Save in the DB for caching
                                AsyncTask.execute(new Runnable() {

                                    @Override
                                    public void run() {

                                        ArrayList<UserEntity> usersFromNetwork = response.body();

                                        userDAO.updateUsers(usersFromNetwork.toArray(new UserEntity[usersFromNetwork.size()]));

                                        listener.OnGetUsersFinishSuccess(usersFromNetwork);

                                    }

                                });


                            }
                            catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<ArrayList<UserEntity>> call, Throwable t) {
                            Timber.e("Failed to fetch users from the server.");
                            listener.OnGetUsersFinishError();
                        }

                    });

                }

            }

        });

    }

    @Override
    public void getUserProfileDetails(final OnGetUserProfileDetailsFinishListener listener, final int userId) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                // First fetch from the DB
                final UserEntity userFromDb = userDAO.findById(userId);

                if(userFromDb != null && !hardReloadDataFromNetwork) {

                    listener.OnGetUserProfileDetailsSuccess(userFromDb);

                }
                // If nothing in DB or user has clicked refresh button then go to network
                else {

                    Call<ArrayList<UserEntity>> call = userService.fetchApplicationUsers();

                    call.enqueue(new Callback<ArrayList<UserEntity>>() {

                        @Override
                        public void onResponse(final Call<ArrayList<UserEntity>> call, final Response<ArrayList<UserEntity>> response) {
                            try {

                                // Save in the DB for caching
                                AsyncTask.execute(new Runnable() {

                                    @Override
                                    public void run() {

                                        ArrayList<UserEntity> usersFromNetwork = response.body();

                                        userDAO.updateUsers(usersFromNetwork.toArray(new UserEntity[usersFromNetwork.size()]));

                                        UserEntity currentLoggedInUser = null;

                                        for (UserEntity userFromNetwork : usersFromNetwork) {

                                            if(userFromNetwork.getId() == userId) {

                                                currentLoggedInUser = userFromNetwork;
                                                break;

                                            }

                                        }

                                        if(currentLoggedInUser != null) {
                                            listener.OnGetUserProfileDetailsSuccess(currentLoggedInUser);
                                        }
                                        else {
                                            onFailure(call, new Exception("Could not find current logged in user details."));
                                        }
                                    }

                                });


                            }
                            catch (Exception e) {
                                onFailure(call, e);
                            }
                        }

                        @Override
                        public void onFailure(Call<ArrayList<UserEntity>> call, Throwable t) {
                            Timber.e("Failed to fetch users list from the server.");
                            listener.OnGetUserProfileDetailsError();
                        }

                    });

                }

            }

        });

    }

    @Override
    public void getSavedCredentials(final OnGetSavedCredentialsListener listener) {

        try {

            String username = SharedPreferencesManager.getSignInUsername(this.context);
            String password = SharedPreferencesManager.getSignInPassword(this.context);

            listener.OnGetSavedUserCredentialsSuccess(username, password);

        }
        catch (Exception ex) {

            listener.OnGetSavedUserCredentialsError();

        }

    }

    @Override
    public void validateCredentials(final String username, final String password, final boolean rememberCredentials, final OnValidateCredentialsListener listener) {

        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                Call<SignOnResponse> call = userService.validateUserCredentials(new SignOnRequest(username, password));

                call.enqueue(new Callback<SignOnResponse>() {

                    @Override
                    public void onResponse(Call<SignOnResponse> call, final Response<SignOnResponse> response) {
                        try {

                            if(response.code() == Constants.StatusCodes.UNAUTHORIZED) {

                                // Clear the cached credentials and user id in the shared preferences
                                setUserSharedPreferences(null, null, -1, false);

                                listener.OnValidateSuccess(false);

                            }
                            else {

                                SignOnResponse signOnResponse = response.body();

                                // Save the correct credentials in the shared preferences
                                // Also Save the signed in user id in the SharedPreferencesManager for future reference in profile details fragment
                                setUserSharedPreferences(username, password, signOnResponse.getId(), rememberCredentials);

                                listener.OnValidateSuccess(true);

                            }

                        }
                        catch (Exception e) {
                            onFailure(call, e);
                        }
                    }

                    @Override
                    public void onFailure(Call<SignOnResponse> call, Throwable t) {
                        Timber.e("Failed to validate user credentials from the server.");
                        listener.OnValidateError();
                    }

                });

            }

        });

    }

    private void setUserSharedPreferences(String username, String password, int userId, boolean rememberCredentials) {

        SharedPreferencesManager.setSignInUsername(this.context, username);
        SharedPreferencesManager.setSignInPassword(this.context, password);
        SharedPreferencesManager.setSignedInUserId(this.context, userId);
        SharedPreferencesManager.setRememberCredentialsPreference(this.context, rememberCredentials);

    }

}