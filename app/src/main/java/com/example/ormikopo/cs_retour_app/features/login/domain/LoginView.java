package com.example.ormikopo.cs_retour_app.features.login.domain;

public interface LoginView {

    void fillSavedCredentials(String username, String password);

    void showInvalidCredentialsMessage();

    void navigateToMainActivity();

    void showGeneralError();

}