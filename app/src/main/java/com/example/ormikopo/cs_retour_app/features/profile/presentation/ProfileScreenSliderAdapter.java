package com.example.ormikopo.cs_retour_app.features.profile.presentation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * A simple pager adapter that represents N ScreenSlidePageFragment objects, in
 * sequence.
 */
public class ProfileScreenSliderAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public ProfileScreenSliderAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:

                UserProfileDetailsFragment userProfileDetailsTab = UserProfileDetailsFragment.newInstance();

                return userProfileDetailsTab;

            case 1:

                UserListFragment userListTab = UserListFragment.newInstance();

                return userListTab;

            default:

                return null;

        }

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}