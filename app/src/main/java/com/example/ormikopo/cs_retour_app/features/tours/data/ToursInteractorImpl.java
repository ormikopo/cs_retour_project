package com.example.ormikopo.cs_retour_app.features.tours.data;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.tours.domain.TourDomain;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursInteractor;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursRepository;

import java.util.ArrayList;

public class ToursInteractorImpl implements ToursInteractor, ToursRepository.OnGetToursListener {

    private ToursInteractor.OnToursFinishListener toursListener;
    private ToursRepository toursRepository;

    public ToursInteractorImpl(ToursInteractor.OnToursFinishListener toursListener, Context context, boolean hardReloadDataFromNetwork) {
        this.toursListener = toursListener;
        this.toursRepository = new ToursRepositoryImpl(context, hardReloadDataFromNetwork);
    }

    @Override
    public void getTours(String tourPackageId) {
        this.toursRepository.getTours(this, tourPackageId);
    }

    @Override
    public void OnGetToursSuccess(ArrayList<TourEntity> tours) {

        ArrayList<TourDomain> result = new ArrayList<>();

        for (TourEntity tour : tours) {

            result.add(
                    new TourDomain(
                            String.valueOf(tour.getId()),
                            tour.getTitle(),
                            tour.getDescription(),
                            tour.getPhotoUrl(),
                            tour.getPrice(),
                            tour.getDuration()
                    )
            );

        }

        this.toursListener.OnSuccess(result);

    }

    @Override
    public void OnGetToursError() {
        this.toursListener.OnError();
    }

}