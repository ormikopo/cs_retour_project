package com.example.ormikopo.cs_retour_app.features.profile.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.example.ormikopo.cs_retour_app.base.GenericDAO;

import java.util.List;

@Dao
public abstract class UserDAO extends GenericDAO<UserEntity> {

    @Query("SELECT * FROM customers")
    public abstract List<UserEntity> getAll();

    @Query("SELECT * FROM customers WHERE id = :id")
    public abstract UserEntity findById(int id);

    @Transaction
    public void updateUsers(UserEntity... userEntities) {
        deleteAll();
        insertMany(userEntities);
    }

    @Query("DELETE FROM customers")
    public abstract void deleteAll();

}