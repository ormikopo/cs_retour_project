package com.example.ormikopo.cs_retour_app.features.makereview.domain;

public interface MakeReviewPresenter {

    void saveUserReview(String reviewText, int ratingValue, String tourPackageId);

}