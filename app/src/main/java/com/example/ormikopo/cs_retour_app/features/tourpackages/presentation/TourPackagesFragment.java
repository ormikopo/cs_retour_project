package com.example.ormikopo.cs_retour_app.features.tourpackages.presentation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.base.MainView;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.OnTourPackageClickListener;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageRegion;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesPresenter;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesView;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class TourPackagesFragment extends Fragment implements TourPackagesView {

    private TourPackagesPresenter presenter;
    private MainView mainView;

    @BindView(R.id.tour_packages_rv)
    RecyclerView tourPackagesRv;

    @BindView(R.id.regions_spinner)
    Spinner mSpinner;

    @BindView(R.id.empty_tour_packages)
    TextView mEmptyTourPackageView;

    public TourPackagesFragment() {
        // Required empty public constructor
    }

    public static TourPackagesFragment newInstance(MainView mainView) {

        TourPackagesFragment mFragment = new TourPackagesFragment();

        // this will stay in memory even if the fragment is destroyed - the fragment will get the tour package from this bundle
        Bundle args = new Bundle();

        args.putSerializable(Constants.BundleKeys.MAIN_VIEW, mainView);
        args.putSerializable(Constants.BundleKeys.HARD_RELOAD, false);

        mFragment.setArguments(args);

        return mFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.mainView = (MainView) getArguments().getSerializable(Constants.BundleKeys.MAIN_VIEW);
        boolean doHardDataReload = getArguments().getBoolean(Constants.BundleKeys.HARD_RELOAD) || false;

        // For resetting the flag if the user will press back button
        if(doHardDataReload) {
            Utilities.resetHardDataReloadFlag(this);
        }

        View view = inflater.inflate(R.layout.fragment_tour_packages, container, false);

        ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        this.tourPackagesRv.setLayoutManager(layoutManager);

        // Initialize Bind Adapter with empty list and null listener
        // For getting rid of error: RecyclerView: No adapter attached; skipping layout in logcat
        // caused because the data comes from another thread and with a small delay
        this.tourPackagesRv.setAdapter(new TourPackagesRvAdapter(new ArrayList<TourPackageUI>(), null, getActivity()));

        this.presenter = new TourPackagesPresenterImpl(this, getActivity(), doHardDataReload);

        SetupRegionSpinner();

        getActivity().setTitle(R.string.tour_packages_list_title);

        return view;
    }

    @Override
    public void showTourPackages(final ArrayList<TourPackageUI> tourPackages) {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                if (tourPackages.isEmpty()) {

                    tourPackagesRv.setVisibility(View.GONE);
                    mEmptyTourPackageView.setVisibility(View.VISIBLE);

                }
                else {

                    tourPackagesRv.setAdapter(new TourPackagesRvAdapter(tourPackages, new OnTourPackageClickListener() {

                        @Override
                        public void onTourPackageClicked(TourPackageUI tourPackage) {

                            // Create ToursFragment and give it an argument specifying the tour package details it should show
                            mainView.addToursFragment(tourPackage);

                        }

                        @Override
                        public void OnTourPackageRateButtonClicked(TourPackageUI tourPackage) {

                            // Create MakeReviewFragment and give it an argument specifying the tour package details it should show
                            mainView.addMakeReviewFragment(tourPackage);

                        }

                    }, getActivity()));

                    tourPackagesRv.setVisibility(View.VISIBLE);
                    mEmptyTourPackageView.setVisibility(View.GONE);

                }

            }

        });

    }

    @Override
    public void showGeneralError() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();

            }

        });

    }

    private void SetupRegionSpinner() {

        // Set Spinner
        // Create an ArrayAdapter using the TourPackageRegion enum values and a default spinner layout
        ArrayAdapter<TourPackageRegion> adapter = new ArrayAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item,
                TourPackageRegion.values()
        );

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        mSpinner.setAdapter(adapter);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                // An item was selected.
                TourPackageRegion selectedRegion = (TourPackageRegion)parent.getItemAtPosition(pos);

                if(selectedRegion == TourPackageRegion.ALL) {
                    presenter.getTourPackages();
                }
                else {
                    presenter.getFilteredTourPackages(selectedRegion);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                presenter.getTourPackages();
            }

        });

    }

}