package com.example.ormikopo.cs_retour_app.rest;

import com.example.ormikopo.cs_retour_app.features.tours.data.TourEntity;
import com.example.ormikopo.cs_retour_app.utils.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TourService {

    @GET(Constants.RestApiEndpoints.GET_TOURS)
    Call<ArrayList<TourEntity>> fetchAvailableToursOfTourPackage(@Path("tourPackageId") String tourPackageId);

}