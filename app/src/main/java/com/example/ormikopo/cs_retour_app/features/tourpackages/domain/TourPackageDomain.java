package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

public class TourPackageDomain {

    private String id;
    private String name;
    private float rating;
    private TourPackageRegion region;
    private String photoUrl;

    public TourPackageDomain(String id, String name, float rating, TourPackageRegion region, String photoUrl) {
        this.id = id;
        this.name = name;
        this.rating = rating;
        this.region = region;
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public TourPackageRegion getRegion() {
        return region;
    }

    public void setRegion(TourPackageRegion region) {
        this.region = region;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

}