package com.example.ormikopo.cs_retour_app.features.makereview.data;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.makereview.domain.MakeReviewInteractor;
import com.example.ormikopo.cs_retour_app.features.reviews.data.ReviewsRepositoryImpl;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsRepository;

public class MakeReviewInteractorImpl implements MakeReviewInteractor, ReviewsRepository.OnCreateReviewListener {

    private MakeReviewInteractor.OnMakeReviewFinishListener createReviewListener;
    private ReviewsRepository reviewsRepository;

    public MakeReviewInteractorImpl(MakeReviewInteractor.OnMakeReviewFinishListener createReviewListener, Context context, boolean hardReloadDataFromNetwork) {
        this.createReviewListener = createReviewListener;
        this.reviewsRepository = new ReviewsRepositoryImpl(context, hardReloadDataFromNetwork);
    }

    @Override
    public void saveReview(String reviewText, int reviewValue, String tourPackageId) {
        this.reviewsRepository.saveReview(this, reviewText, reviewValue, tourPackageId);
    }

    @Override
    public void OnCreateReviewSuccess() {
        this.createReviewListener.OnSuccess();
    }

    @Override
    public void OnCreateReviewError() {
        this.createReviewListener.OnError();
    }

}