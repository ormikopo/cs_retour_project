package com.example.ormikopo.cs_retour_app.features.reviews.presentation;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.base.MainView;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewUI;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsPresenter;
import com.example.ormikopo.cs_retour_app.features.reviews.domain.ReviewsView;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFragment extends Fragment implements ReviewsView {

    private ReviewsPresenter presenter;
    private MainView mainView;

    @BindView(R.id.reviews_rv)
    RecyclerView reviewsRv;

    @BindView(R.id.tour_package_name)
    TextView mTourPackageName;

    @BindView(R.id.tour_package_region)
    TextView mTourPackageRegion;

    @BindView(R.id.filter_review_text)
    EditText mFilterEditText;

    @BindView(R.id.empty_reviews)
    TextView mEmptyReviewsText;

    public ReviewsFragment() {
        // Required empty public constructor
    }

    public static ReviewsFragment newInstance(MainView mainView, TourPackageUI tourPackage) {

        ReviewsFragment mFragment = new ReviewsFragment();

        // this will stay in memory even if the fragment is destroyed - the fragment will get the tour package from this bundle
        Bundle args = new Bundle();

        args.putParcelable(Constants.ParcelableKeys.TOUR_PACKAGE, tourPackage);
        args.putSerializable(Constants.BundleKeys.MAIN_VIEW, mainView);
        args.putSerializable(Constants.BundleKeys.HARD_RELOAD, false);

        mFragment.setArguments(args);

        return mFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        TourPackageUI tourPackage = getArguments().getParcelable(Constants.ParcelableKeys.TOUR_PACKAGE);
        this.mainView = (MainView) getArguments().getSerializable(Constants.BundleKeys.MAIN_VIEW);
        boolean doHardDataReload = getArguments().getBoolean(Constants.BundleKeys.HARD_RELOAD) || false;

        // For resetting the flag if the user will press back button
        if(doHardDataReload) {
            Utilities.resetHardDataReloadFlag(this);
        }

        View view = inflater.inflate(R.layout.fragment_reviews, container, false);

        ButterKnife.bind(this, view);

        // Show Tour Package Details
        this.mTourPackageName.setText(tourPackage.getName());
        this.mTourPackageRegion.setText(tourPackage.getRegion().toString());
        this.mTourPackageRegion.setTextColor(getActivity().getResources().getColor(tourPackage.getRegionColor()));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        this.reviewsRv.setLayoutManager(layoutManager);

        // Initialize Bind Adapter with empty list and null listener
        // For getting rid of error: RecyclerView: No adapter attached; skipping layout in logcat
        // caused because the data comes from another thread and with a small delay
        this.reviewsRv.setAdapter(new ReviewsRvAdapter(new ArrayList<ReviewUI>(), null));

        this.presenter = new ReviewsPresenterImpl(this, getActivity(), doHardDataReload);

        this.presenter.getReviews(tourPackage.getId());

        getActivity().setTitle(R.string.reviews_and_ratings_title);

        return view;

    }

    @Override
    public void showReviews(final ArrayList<ReviewUI> reviews) {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                if (reviews.isEmpty()) {

                    reviewsRv.setVisibility(View.GONE);
                    mEmptyReviewsText.setVisibility(View.VISIBLE);

                }
                else {

                    reviewsRv.setAdapter(new ReviewsRvAdapter(reviews, getActivity()));

                    reviewsRv.setVisibility(View.VISIBLE);
                    mEmptyReviewsText.setVisibility(View.GONE);

                }

            }

        });

    }

    @Override
    public void showGeneralError() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();

            }

        });

    }

    @OnClick(R.id.filter_review_button)
    public void filterReviews(View view) {

        TourPackageUI tourPackage = getArguments().getParcelable(Constants.ParcelableKeys.TOUR_PACKAGE);

        String filterText = this.mFilterEditText.getText().toString();

        this.presenter.getFilteredReviews(tourPackage.getId(), filterText);

    }



    @OnClick(R.id.make_a_new_review_button)
    public void rateTourPackage(View view) {

        TourPackageUI tourPackage = getArguments().getParcelable(Constants.ParcelableKeys.TOUR_PACKAGE);

        // Create MakeReviewFragment and give it an argument specifying the tour package details it should show
        this.mainView.addMakeReviewFragment(tourPackage);

    }

}