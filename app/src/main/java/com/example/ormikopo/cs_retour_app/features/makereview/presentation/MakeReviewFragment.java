package com.example.ormikopo.cs_retour_app.features.makereview.presentation;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.makereview.domain.MakeReviewPresenter;
import com.example.ormikopo.cs_retour_app.features.makereview.domain.MakeReviewView;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;
import com.example.ormikopo.cs_retour_app.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MakeReviewFragment extends Fragment implements MakeReviewView {

    private MakeReviewPresenter presenter;
    private String reviewText;
    private int reviewValue;
    private String tourPackageId;

    @BindView(R.id.ratingBar)
    RatingBar mRatingBar;

    @BindView(R.id.tvRatingScale)
    TextView mTextReview;

    @BindView(R.id.tour_package_review_wrapper)
    TextInputLayout mReviewInputTextWrapper;

    @BindView(R.id.tour_package_review_input)
    EditText mReviewInputText;

    @BindView(R.id.tour_package_name)
    TextView mTourPackageName;

    @BindView(R.id.tour_package_region)
    TextView mTourPackageRegion;

    public MakeReviewFragment() {
        // Required empty public constructor
    }

    public static MakeReviewFragment newInstance(TourPackageUI tourPackage) {

        MakeReviewFragment mFragment = new MakeReviewFragment();

        // this will stay in memory even if the fragment is destroyed - the fragment will get the tour package from this bundle
        Bundle args = new Bundle();

        args.putParcelable(Constants.ParcelableKeys.TOUR_PACKAGE, tourPackage);

        mFragment.setArguments(args);

        return mFragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TourPackageUI tourPackage = getArguments().getParcelable(Constants.ParcelableKeys.TOUR_PACKAGE);
        this.tourPackageId = tourPackage.getId();

        View view = inflater.inflate(R.layout.fragment_make_review, container, false);

        ButterKnife.bind(this, view);

        // Show Tour Package Details
        this.mTourPackageName.setText(tourPackage.getName());
        this.mTourPackageRegion.setText(tourPackage.getRegion().toString());
        this.mTourPackageRegion.setTextColor(getActivity().getResources().getColor(tourPackage.getRegionColor()));

        this.presenter = new MakeReviewPresenterImpl(this, getActivity().getApplicationContext() );

        addListenerOnRatingBar();

        getActivity().setTitle(R.string.make_review_screen_title);

        return view;
    }

    @OnClick(R.id.submit_button)
    public void submit(){

        this.reviewText = this.mReviewInputText.getText().toString();

        if(this.reviewText.equals("")) {
            this.mReviewInputTextWrapper.setError(getResources().getString(R.string.review_text_required));
        }
        else {
            this.mReviewInputTextWrapper.setError(null);
        }

        if(this.reviewValue == 0) {
            this.mTextReview.setTextColor(getResources().getColor(R.color.colorRed));
            this.mTextReview.setText(getResources().getString(R.string.rating_choice_required));
        }

        if(this.reviewText.equals("") || this.reviewValue == 0) {
            return;
        }

        this.presenter.saveUserReview(this.reviewText, this.reviewValue, this.tourPackageId);

    }

    @Override
    public void showSuccessMessage() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                mReviewInputText.setText(null);
                mRatingBar.setRating(0);

                Toast.makeText(getContext(), getString(R.string.make_review_success_message), Toast.LENGTH_LONG).show();

            }

        });

    }

    @Override
    public void showGeneralError() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();

            }

        });

    }

    @OnClick(R.id.ratingBar)
    public void addListenerOnRatingBar() {

        this.mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                int colorId = R.color.colorPrimary;

                switch ((int) ratingBar.getRating()) {
                    case 1:
                        mTextReview.setText("Very bad");
                        reviewValue = 1;
                        colorId = R.color.colorRed;
                        break;
                    case 2:
                        mTextReview.setText("Need some improvement");
                        reviewValue = 2;
                        colorId = R.color.colorRed;
                        break;
                    case 3:
                        mTextReview.setText("Good");
                        reviewValue = 3;
                        colorId = R.color.colorYellow;
                        break;
                    case 4:
                        mTextReview.setText("Great");
                        reviewValue = 4;
                        colorId = R.color.colorGreen;
                        break;
                    case 5:
                        mTextReview.setText("Awesome. I love it");
                        reviewValue = 5;
                        colorId = R.color.colorGreen;
                        break;
                    default:
                        mTextReview.setText("");
                }

                mTextReview.setTextColor(getActivity().getResources().getColor(colorId));

            }

        });

    }

}