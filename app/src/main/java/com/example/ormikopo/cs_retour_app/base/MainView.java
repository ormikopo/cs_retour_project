package com.example.ormikopo.cs_retour_app.base;

import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;

import java.io.Serializable;

public interface MainView extends Serializable {

    void addTourPackagesFragment();

    void addToursFragment(TourPackageUI tourPackage);

    void addReviewsFragment(TourPackageUI tourPackage);

    void addMakeReviewFragment(TourPackageUI tourPackage);

    void addProfileFragment();

}