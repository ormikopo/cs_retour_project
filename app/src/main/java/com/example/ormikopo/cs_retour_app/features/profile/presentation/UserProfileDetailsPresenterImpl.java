package com.example.ormikopo.cs_retour_app.features.profile.presentation;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.profile.data.UserProfileDetailsInteractorImpl;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserDomain;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserProfileDetailsInteractor;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserProfileDetailsPresenter;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserProfileDetailsView;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserUI;

public class UserProfileDetailsPresenterImpl implements UserProfileDetailsPresenter, UserProfileDetailsInteractor.OnUserProfileDetailsFinishListener {

    private UserProfileDetailsView userProfileDetailsView;
    private UserProfileDetailsInteractor interactor;

    public UserProfileDetailsPresenterImpl(UserProfileDetailsView userProfileDetailsView, Context context, boolean hardReloadDataFromNetwork) {
        this.userProfileDetailsView = userProfileDetailsView;
        this.interactor = new UserProfileDetailsInteractorImpl(this, context, hardReloadDataFromNetwork);
    }

    @Override
    public void getUserProfileDetails(int userId) {
        this.interactor.getUserProfileDetails(userId);
    }

    @Override
    public void OnSuccess(UserDomain user) {

        UserUI userUI = new UserUI(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getAddress(),
                user.getAge(),
                user.getTeam()
        );

        String userTeam = user.getTeam();

        if (userTeam.equals("BLUE")) {
            userUI.setTeamColor(R.color.colorBlue);
        }
        else if(userTeam.equals("YELLOW")) {
            userUI.setTeamColor(R.color.colorYellow);
        }
        else if(userTeam.equals("BLACK")) {
            userUI.setTeamColor(R.color.colorBlack);
        }
        else if(userTeam.equals("RED")) {
            userUI.setTeamColor(R.color.colorRed);
        }
        else {
            userUI.setTeamColor(R.color.colorPrimary);
        }

        this.userProfileDetailsView.showUserProfileDetails(userUI);

    }

    @Override
    public void OnError() {
        this.userProfileDetailsView.showGeneralError();
    }

}