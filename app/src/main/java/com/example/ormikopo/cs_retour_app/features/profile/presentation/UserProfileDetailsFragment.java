package com.example.ormikopo.cs_retour_app.features.profile.presentation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserProfileDetailsPresenter;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserProfileDetailsView;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserUI;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.SharedPreferencesManager;
import com.example.ormikopo.cs_retour_app.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileDetailsFragment extends Fragment implements UserProfileDetailsView {

    private UserProfileDetailsPresenter presenter;
    private boolean isFragmentVisible;

    @BindView(R.id.username)
    EditText mUsername;

    @BindView(R.id.fullName)
    EditText mFullName;

    @BindView(R.id.email)
    EditText mEmail;

    @BindView(R.id.address)
    EditText mAddress;

    @BindView(R.id.age)
    EditText mAge;

    @BindView(R.id.user_team)
    TextView mUserTeam;

    @BindView(R.id.user_profile_details_view)
    View mView;

    public UserProfileDetailsFragment() {
        // Required empty public constructor
    }

    public static UserProfileDetailsFragment newInstance() {
        return new UserProfileDetailsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        boolean doHardDataReload = false;

        // For resetting the flag if the user will press back button
        if(this.isFragmentVisible) {

            doHardDataReload = getParentFragment().getArguments().getBoolean(Constants.BundleKeys.HARD_RELOAD) || false;

            if(doHardDataReload) {

                Utilities.resetHardDataReloadFlag(getParentFragment());

            }

        }

        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        ButterKnife.bind(this, view);

        this.presenter = new UserProfileDetailsPresenterImpl(this, getActivity(), doHardDataReload);

        this.presenter.getUserProfileDetails(SharedPreferencesManager.getSignedInUserId(getActivity()));

        return view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);

        this.isFragmentVisible = isVisibleToUser;

    }

    @Override
    public void showUserProfileDetails(final UserUI user) {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                mUsername.setText(user.getUsername());
                mFullName.setText(user.getFirstName() + " " + user.getLastName());
                mEmail.setText(user.getEmail());
                mAddress.setText(user.getAddress());
                mAge.setText(String.valueOf(user.getAge()));
                mView.setBackgroundColor(getActivity().getResources().getColor(user.getTeamColor()));
                mUserTeam.setText("Team " + user.getTeam());

            }

        });

    }

    @Override
    public void showGeneralError() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();

            }

        });

    }

}