package com.example.ormikopo.cs_retour_app.features.tours.domain;

public interface ToursPresenter {

    void getTours(String tourPackageId);

}