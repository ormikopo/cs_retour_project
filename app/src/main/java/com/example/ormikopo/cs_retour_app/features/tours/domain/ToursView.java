package com.example.ormikopo.cs_retour_app.features.tours.domain;

import java.util.ArrayList;

public interface ToursView {

    void showTours(ArrayList<TourUI> tours);

    void showGeneralError();

}