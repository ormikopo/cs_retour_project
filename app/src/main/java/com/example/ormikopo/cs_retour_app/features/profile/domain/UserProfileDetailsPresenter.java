package com.example.ormikopo.cs_retour_app.features.profile.domain;

public interface UserProfileDetailsPresenter {

    void getUserProfileDetails(int userId);

}