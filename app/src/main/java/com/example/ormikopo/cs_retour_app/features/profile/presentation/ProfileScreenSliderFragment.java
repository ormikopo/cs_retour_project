package com.example.ormikopo.cs_retour_app.features.profile.presentation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileScreenSliderFragment extends Fragment {

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    @BindView(R.id.profile_pager)
    ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    public ProfileScreenSliderFragment() {
        // Required empty public constructor
    }

    public static ProfileScreenSliderFragment newInstance() {

        ProfileScreenSliderFragment mFragment = new ProfileScreenSliderFragment();

        // this will stay in memory even if the fragment is destroyed
        Bundle args = new Bundle();

        args.putSerializable(Constants.BundleKeys.HARD_RELOAD, false);

        mFragment.setArguments(args);

        return mFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_profile_screen_slider, container, false);

        ButterKnife.bind(this, rootView);

        // Instantiate a ViewPager and a PagerAdapter.
        mPagerAdapter = new ProfileScreenSliderAdapter(getChildFragmentManager(), 2);
        mPager.setAdapter(mPagerAdapter);

        getActivity().setTitle(R.string.profile_pager_title);

        return rootView;

    }

}