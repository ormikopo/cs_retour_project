package com.example.ormikopo.cs_retour_app.rest;

import com.example.ormikopo.cs_retour_app.features.profile.data.UserEntity;
import com.example.ormikopo.cs_retour_app.rest.requests.SignOnRequest;
import com.example.ormikopo.cs_retour_app.rest.responses.SignOnResponse;
import com.example.ormikopo.cs_retour_app.utils.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UserService {

    @GET(Constants.RestApiEndpoints.GET_USERS)
    Call<ArrayList<UserEntity>> fetchApplicationUsers();

    @POST(Constants.RestApiEndpoints.VALIDATE_USER_CREDENTIALS)
    Call<SignOnResponse> validateUserCredentials(@Body SignOnRequest userCredentials);

}