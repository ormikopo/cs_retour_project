package com.example.ormikopo.cs_retour_app.features.login.presentation;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ormikopo.cs_retour_app.base.MainActivity;
import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.login.domain.LoginPresenter;
import com.example.ormikopo.cs_retour_app.features.login.domain.LoginView;
import com.example.ormikopo.cs_retour_app.utils.SharedPreferencesManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements LoginView {

    private LoginPresenter presenter;

    // Important Note:
    // Activity / Fragment class properties with m in front -> View Properties
    // Activity / Fragment class properties without m in front -> Plain Class Properties

    @BindView(R.id.username_wrapper)
    TextInputLayout mUsernameWrapper;

    @BindView(R.id.username)
    EditText mUsername;

    @BindView(R.id.password_wrapper)
    TextInputLayout mPasswordWrapper;

    @BindView(R.id.password)
    EditText mPassword;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.remember_credentials)
    CheckBox mCheckBox;

    @BindView(R.id.login_button)
    Button mLoginButton;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        ButterKnife.bind(this, view);

        this.mProgressBar.setVisibility(View.GONE);

        this.presenter = new LoginPresenterImpl(this, getActivity());

        boolean rememberCredentials = SharedPreferencesManager.getRememberCredentialsPreference(getActivity());

        if(rememberCredentials) {
            this.presenter.getSavedCredentials();
        }

        this.mCheckBox.setChecked(rememberCredentials);

        return view;

    }

    @OnClick(R.id.login_button)
    public void login(View view) {

        String username = this.mUsername.getText().toString();

        if(username.equals("")) {
            this.mUsernameWrapper.setError(getResources().getString(R.string.username_required));
        }
        else {
            this.mUsernameWrapper.setError(null);
        }

        String password = this.mPassword.getText().toString();

        if(password.equals("")) {
            this.mPasswordWrapper.setError(getResources().getString(R.string.password_required));
        }
        else {
            this.mPasswordWrapper.setError(null);
        }

        if(username.equals("") || password.equals("")) {
            return;
        }

        this.mProgressBar.setVisibility(View.VISIBLE);
        this.mLoginButton.setVisibility(View.GONE);

        this.presenter.validateCredentials(username, password, this.mCheckBox.isChecked());

    }

    @Override
    public void fillSavedCredentials(String username, String password) {

        if(username != null && password != null) {

            this.mUsername.setText(username);
            this.mPassword.setText(password);

        }

    }

    @Override
    public void showInvalidCredentialsMessage() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                mProgressBar.setVisibility(View.GONE);
                mLoginButton.setVisibility(View.VISIBLE);

                Toast.makeText(getActivity(), getResources().getString(R.string.invalid_credentials_msg), Toast.LENGTH_SHORT).show();

            }

        });

    }

    @Override
    public void navigateToMainActivity() {

        FragmentActivity currentActivity = getActivity();

        startActivity(new Intent(currentActivity, MainActivity.class));

        getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        // for ending the current activity that this fragment is hooked on
        currentActivity.finish();

    }

    @Override
    public void showGeneralError() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                mProgressBar.setVisibility(View.GONE);
                mLoginButton.setVisibility(View.VISIBLE);

                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();

            }

        });

    }

}