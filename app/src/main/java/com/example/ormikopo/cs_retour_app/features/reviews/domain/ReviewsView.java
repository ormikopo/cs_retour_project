package com.example.ormikopo.cs_retour_app.features.reviews.domain;

import java.util.ArrayList;

public interface ReviewsView {

    void showReviews(ArrayList<ReviewUI> reviews);

    void showGeneralError();

}