package com.example.ormikopo.cs_retour_app.features.profile.domain;

import com.example.ormikopo.cs_retour_app.features.profile.data.UserEntity;

import java.util.ArrayList;

public interface UserRepository {

    void getUsers(OnGetUsersFinishListener listener);

    interface OnGetUsersFinishListener {

        void OnGetUsersFinishSuccess(ArrayList<UserEntity> users);

        void OnGetUsersFinishError();

    }

    void getUserProfileDetails(OnGetUserProfileDetailsFinishListener listener, int userId);

    interface OnGetUserProfileDetailsFinishListener {

        void OnGetUserProfileDetailsSuccess(UserEntity user);

        void OnGetUserProfileDetailsError();

    }

    void getSavedCredentials(OnGetSavedCredentialsListener listener);

    interface OnGetSavedCredentialsListener {

        void OnGetSavedUserCredentialsSuccess(String username, String password);

        void OnGetSavedUserCredentialsError();

    }

    void validateCredentials(String username, String password, boolean rememberCredentials, OnValidateCredentialsListener listener);

    interface OnValidateCredentialsListener {

        void OnValidateSuccess(boolean validationResult);

        void OnValidateError();

    }

}