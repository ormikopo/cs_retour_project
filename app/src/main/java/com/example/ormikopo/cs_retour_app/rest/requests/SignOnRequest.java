package com.example.ormikopo.cs_retour_app.rest.requests;

public class SignOnRequest {

    private String username;
    private String password;

    public SignOnRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}