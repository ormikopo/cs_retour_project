package com.example.ormikopo.cs_retour_app.features.tours.domain;

import java.util.ArrayList;

public interface ToursInteractor {

    void getTours(String tourPackageId);

    interface OnToursFinishListener {

        void OnSuccess(ArrayList<TourDomain> tours);

        void OnError();

    }

}