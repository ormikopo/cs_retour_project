package com.example.ormikopo.cs_retour_app.features.reviews.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackageEntity;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "reviews",
        indices = {@Index("username"), @Index("tour_package_id")},
        foreignKeys = {
                @ForeignKey(
                        entity = TourPackageEntity.class,
                        parentColumns = "id",
                        childColumns = "tour_package_id",
                        onDelete = CASCADE
                )
        }
)
public class ReviewEntity {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    @ColumnInfo(name = "score")
    private int score;

    @ColumnInfo(name = "comment")
    private String comment;

    @ColumnInfo(name = "username")
    private String username;

    @ColumnInfo(name = "tour_package_id")
    private String tourPackageId;

    public ReviewEntity(int score, String comment, String username, String tourPackageId) {
        this.score = score;
        this.comment = comment;
        this.username = username;
        this.tourPackageId = tourPackageId;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTourPackageId() {
        return tourPackageId;
    }

    public void setTourPackageId(String tourPackageId) {
        this.tourPackageId = tourPackageId;
    }

}