package com.example.ormikopo.cs_retour_app.features.tours.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackageEntity;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "tours",
        indices = {@Index("title"), @Index("tour_package_id")},
        foreignKeys = @ForeignKey(
                entity = TourPackageEntity.class,
                parentColumns = "id",
                childColumns = "tour_package_id",
                onDelete = CASCADE
        )
)
public class TourEntity {

    @PrimaryKey
    @NonNull
    private int id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "price")
    private int price;

    @ColumnInfo(name = "duration")
    private String duration;

    @ColumnInfo(name = "bullets")
    private String bullets;

    @ColumnInfo(name = "keywords")
    private String keywords;

    @ColumnInfo(name = "photo_url")
    private String photoUrl;

    @ColumnInfo(name = "tour_package_id")
    private String tourPackageId;

    public TourEntity(@NonNull int id, String title, String description, int price, String duration, String bullets, String keywords, String photoUrl, String tourPackageId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.bullets = bullets;
        this.keywords = keywords;
        this.photoUrl = photoUrl;
        this.tourPackageId = tourPackageId;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBullets() {
        return bullets;
    }

    public void setBullets(String bullets) {
        this.bullets = bullets;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getTourPackageId() {
        return tourPackageId;
    }

    public void setTourPackageId(String tourPackageId) {
        this.tourPackageId = tourPackageId;
    }

}