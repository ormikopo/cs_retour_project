package com.example.ormikopo.cs_retour_app.features.tours.presentation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.base.MainView;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;
import com.example.ormikopo.cs_retour_app.features.tours.domain.OnTourClickListener;
import com.example.ormikopo.cs_retour_app.features.tours.domain.TourUI;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursPresenter;
import com.example.ormikopo.cs_retour_app.features.tours.domain.ToursView;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ToursFragment extends Fragment implements ToursView {

    private ToursPresenter presenter;
    private MainView mainView;

    @BindView(R.id.tours_rv)
    RecyclerView toursRv;

    @BindView(R.id.tour_package_name)
    TextView mTourPackageName;

    @BindView(R.id.tour_package_region)
    TextView mTourPackageRegion;

    @BindView(R.id.tour_package_rating)
    RatingBar mTourPackageRating;

    @BindView(R.id.rating_text_value)
    TextView mRatingTextValue;

    @BindView(R.id.empty_tours)
    TextView mEmptyToursView;

    public ToursFragment() {
        // Required empty public constructor
    }

    public static ToursFragment newInstance(MainView mainView, TourPackageUI tourPackage) {

        ToursFragment mFragment = new ToursFragment();

        // this will stay in memory even if the fragment is destroyed - the fragment will get the tour package from this bundle
        Bundle args = new Bundle();

        args.putParcelable(Constants.ParcelableKeys.TOUR_PACKAGE, tourPackage);
        args.putSerializable(Constants.BundleKeys.MAIN_VIEW, mainView);
        args.putSerializable(Constants.BundleKeys.HARD_RELOAD, false);

        mFragment.setArguments(args);

        return mFragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        TourPackageUI tourPackage = getArguments().getParcelable(Constants.ParcelableKeys.TOUR_PACKAGE);
        this.mainView = (MainView) getArguments().getSerializable(Constants.BundleKeys.MAIN_VIEW);
        boolean doHardDataReload = getArguments().getBoolean(Constants.BundleKeys.HARD_RELOAD) || false;

        // For resetting the flag if the user will press back button
        if(doHardDataReload) {
            Utilities.resetHardDataReloadFlag(this);
        }

        View view = inflater.inflate(R.layout.fragment_tours, container, false);

        ButterKnife.bind(this, view);

        // Show Tour Package Details
        this.mTourPackageName.setText(tourPackage.getName());

        this.mTourPackageRegion.setText(tourPackage.getRegion().toString());
        this.mTourPackageRegion.setTextColor(getActivity().getResources().getColor(tourPackage.getRegionColor()));

        this.mTourPackageRating.setRating(tourPackage.getRating());

        float tourPackageRating = tourPackage.getRating();

        if(tourPackageRating == 0.0) {
            this.mRatingTextValue.setText(R.string.zero_rating_message);
        }
        else {
            this.mRatingTextValue.setText("(" + String.valueOf(tourPackageRating) + ")");
        }

        this.mRatingTextValue.setTextColor(getActivity().getResources().getColor(tourPackage.getRatingColor()));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        this.toursRv.setLayoutManager(layoutManager);

        // Initialize Bind Adapter with empty list and null listener
        // For getting rid of error: RecyclerView: No adapter attached; skipping layout in logcat
        // caused because the data comes from another thread and with a small delay
        this.toursRv.setAdapter(new ToursRvAdapter(new ArrayList<TourUI>(), null));

        this.presenter = new ToursPresenterImpl(this, getActivity(), doHardDataReload);

        this.presenter.getTours(tourPackage.getId());

        getActivity().setTitle(R.string.tours_list_title);

        return view;

    }

    @Override
    public void showTours(final ArrayList<TourUI> tours) {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                if (tours.isEmpty()) {

                    toursRv.setVisibility(View.GONE);
                    mEmptyToursView.setVisibility(View.VISIBLE);

                }
                else {

                    toursRv.setAdapter(new ToursRvAdapter(tours, new OnTourClickListener() {

                        @Override
                        public void onTourClicked(TourUI tour) {

                            String title = tour.getName();

                            String message = String.format("<strong>Price</strong>: %d<br><strong>Duration</strong>: %s", tour.getPrice(), tour.getDuration());

                            new AlertDialog
                                    .Builder(getActivity())
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .setTitle(title)
                                    .setMessage(Html.fromHtml(message))
                                    .setNeutralButton(R.string.dismiss_button_text, null)
                                    .show();

                        }

                    }));

                    toursRv.setVisibility(View.VISIBLE);
                    mEmptyToursView.setVisibility(View.GONE);

                }

            }

        });

    }

    @Override
    public void showGeneralError() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();

            }

        });

    }

    @OnClick(R.id.tour_package_reviews_button)
    public void showTourPackageReviews(View view) {

        TourPackageUI tourPackage = getArguments().getParcelable(Constants.ParcelableKeys.TOUR_PACKAGE);

        // Create ReviewsFragment and give it an argument specifying the tour package details it should show
        this.mainView.addReviewsFragment(tourPackage);

    }

    @OnClick(R.id.tour_package_rate_button)
    public void rateTourPackage(View view) {

        TourPackageUI tourPackage = getArguments().getParcelable(Constants.ParcelableKeys.TOUR_PACKAGE);

        // Create MakeReviewFragment and give it an argument specifying the tour package details it should show
        this.mainView.addMakeReviewFragment(tourPackage);

    }
}