package com.example.ormikopo.cs_retour_app.features.profile.presentation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.profile.domain.OnUserClickListener;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserListPresenter;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserListView;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserUI;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserListFragment extends Fragment implements UserListView {

    private UserListPresenter presenter;
    private boolean isFragmentVisible;

    @BindView(R.id.users_rv)
    RecyclerView usersRv;

    public UserListFragment() {
        // Required empty public constructor
    }

    public static UserListFragment newInstance() {
        return new UserListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        boolean doHardDataReload = false;

        // For resetting the flag if the user will press back button
        if(this.isFragmentVisible) {

            doHardDataReload = getParentFragment().getArguments().getBoolean(Constants.BundleKeys.HARD_RELOAD) || false;

            if(doHardDataReload) {

                Utilities.resetHardDataReloadFlag(getParentFragment());

            }

        }

        View view = inflater.inflate(R.layout.fragment_user_list, container, false);

        ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        this.usersRv.setLayoutManager(layoutManager);

        // Initialize Bind Adapter with empty list and null listener
        // For getting rid of error: RecyclerView: No adapter attached; skipping layout in logcat
        // caused because the data comes from another thread and with a small delay
        this.usersRv.setAdapter(new UsersRvAdapter(new ArrayList<UserUI>(), null, getActivity()));

        this.presenter = new UserListPresenterImpl(this, getActivity(), doHardDataReload);

        this.presenter.getUsers();

        return view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);

        this.isFragmentVisible = isVisibleToUser;

    }

    @Override
    public void showUsers(final ArrayList<UserUI> users) {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                usersRv.setAdapter(new UsersRvAdapter(users, new OnUserClickListener() {

                    @Override
                    public void onUserClicked(UserUI user) {

                        String title = user.getUsername() + " - " + user.getTeam();

                        String message = String.format("<strong>Full Name</strong>: %s<br><strong>Age</strong>: %d<br><strong>Address</strong>: %s<br><strong>Email</strong>: %s", user.getFirstName() + " " + user.getLastName() , user.getAge(), user.getAddress(), user.getEmail());

                        new AlertDialog
                                .Builder(getActivity())
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle(title)
                                .setMessage(Html.fromHtml(message))
                                .setNeutralButton(R.string.dismiss_button_text, null)
                                .show();

                    }

                }, getActivity()));

            }

        });

    }

    @Override
    public void showGeneralError() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();

            }

        });

    }

}