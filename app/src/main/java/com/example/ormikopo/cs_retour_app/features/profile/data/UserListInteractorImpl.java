package com.example.ormikopo.cs_retour_app.features.profile.data;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.profile.domain.UserDomain;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserListInteractor;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserRepository;

import java.util.ArrayList;

public class UserListInteractorImpl implements UserListInteractor, UserRepository.OnGetUsersFinishListener {

    private UserListInteractor.OnUserListFinishListener userListListener;
    private UserRepository userRepository;

    public UserListInteractorImpl(UserListInteractor.OnUserListFinishListener userListListener, Context context, boolean hardReloadDataFromNetwork) {
        this.userListListener = userListListener;
        this.userRepository = new UserRepositoryImpl(context, hardReloadDataFromNetwork);
    }

    @Override
    public void getUsers() {
        this.userRepository.getUsers(this);
    }

    @Override
    public void OnGetUsersFinishSuccess(ArrayList<UserEntity> users) {

        ArrayList<UserDomain> result = new ArrayList<>();

        for (UserEntity userFromDb : users) {
            result.add(
                    new UserDomain(
                            String.valueOf(userFromDb.getId()),
                            userFromDb.getUsername(),
                            userFromDb.getFirstName(),
                            userFromDb.getLastName(),
                            userFromDb.getEmail(),
                            userFromDb.getAddress(),
                            userFromDb.getAge(),
                            userFromDb.getTeam()
                    )
            );
        }

        this.userListListener.OnSuccess(result);

    }

    @Override
    public void OnGetUsersFinishError() {
        this.userListListener.OnError();
    }

}