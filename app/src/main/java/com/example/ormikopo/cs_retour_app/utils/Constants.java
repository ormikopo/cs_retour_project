package com.example.ormikopo.cs_retour_app.utils;

public class Constants {

    public static class FragmentTags {

        public static final String MAKE_REVIEW_FRAGMENT = "MAKE_REVIEW_FRAGMENT";

        public static final String TOUR_PACKAGES_FRAGMENT = "TOUR_PACKAGES_FRAGMENT";

        public static final String TOURS_FRAGMENT = "TOURS_FRAGMENT";

        public static final String LOGIN_FRAGMENT = "LOGIN_FRAGMENT";

        public static final String PROFILE_FRAGMENT = "PROFILE_FRAGMENT";

        public static final String REVIEWS_FRAGMENT = "REVIEWS_FRAGMENT";

    }

    public static class RestClientSettings {

        public static final int CONNECT_TIMEOUT = 10;

        public static final int READ_TIMEOUT = 10;

    }

    public static class RestApiEndpoints {

        public static final String BASE_URL = "https://explore-greece.herokuapp.com/";

        public static final String GET_TOUR_PACKAGES = "tourPackages";

        public static final String GET_TOURS = "tourPackages/{tourPackageId}/tours";

        public static final String REVIEWS = "tourPackages/{tourPackageId}/reviews";

        public static final String GET_USERS = "users";

        public static final String VALIDATE_USER_CREDENTIALS = "login";

    }

    public static class ParcelableKeys {

        public static final String TOUR_PACKAGE = "tour_package";

    }

    public static class BundleKeys {

        public static final String HARD_RELOAD = "hard_reload";

        public static final String MAIN_VIEW = "main_view";

    }

    public static class StatusCodes {

        public static final int UNAUTHORIZED = 401;

        public static final int CREATED = 201;

    }

    public static class DbConfig {

        public static final String DATABASE_NAME = "retour_database";

    }

}