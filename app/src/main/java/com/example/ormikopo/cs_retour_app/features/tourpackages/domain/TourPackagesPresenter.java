package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

public interface TourPackagesPresenter {

    void getTourPackages();

    void getFilteredTourPackages(TourPackageRegion filterByRegion);

}