package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackageEntity;

import java.util.ArrayList;

public interface TourPackagesRepository {

    void getTourPackages(OnGetTourPackagesListener listener);

    void getFilteredTourPackages(TourPackageRegion regionEnum, OnGetTourPackagesListener listener);

    interface OnGetTourPackagesListener {

        void OnGetTourPackagesSuccess(ArrayList<TourPackageEntity> tourPackages);

        void OnGetTourPackagesError();

    }

}