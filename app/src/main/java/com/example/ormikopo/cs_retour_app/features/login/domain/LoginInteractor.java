package com.example.ormikopo.cs_retour_app.features.login.domain;

public interface LoginInteractor {

    void getSavedCredentials();

    interface OnGetSavedCredentialsFinishListener {

        void OnGetSavedCredentialsSuccess(String username, String password);

        void OnGetSavedCredentialsError();

    }

    void validateCredentials(String username, String password, boolean rememberCredentials);

    interface OnValidateCredentialsFinishListener {

        void OnValidateCredentialsSuccess(boolean validationResult);

        void OnValidateCredentialsError();

    }

}