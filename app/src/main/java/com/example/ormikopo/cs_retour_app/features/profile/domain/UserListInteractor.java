package com.example.ormikopo.cs_retour_app.features.profile.domain;

import java.util.ArrayList;

public interface UserListInteractor {

    void getUsers();

    interface OnUserListFinishListener {

        void OnSuccess(ArrayList<UserDomain> users);

        void OnError();

    }

}