package com.example.ormikopo.cs_retour_app.base;

import android.app.Application;

import timber.log.Timber;

public class RetourApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize our logging mechanism - Use it as Timber.d("some message");
        Timber.plant(new Timber.DebugTree());
    }

}