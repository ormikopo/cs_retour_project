package com.example.ormikopo.cs_retour_app.features.reviews.domain;

import com.example.ormikopo.cs_retour_app.features.reviews.data.ReviewEntity;

import java.util.ArrayList;

public interface ReviewsRepository {

    void getReviews(OnGetReviewsListener listener, String tourPackageId);

    void getFilteredReviews(OnGetReviewsListener listener, String tourPackageId, String filter);

    interface OnGetReviewsListener {

        void OnGetReviewsSuccess(ArrayList<ReviewEntity> reviews);

        void OnGetReviewsError();

    }

    void saveReview(OnCreateReviewListener listener, String reviewText, int reviewValue, String tourPackageId);

    interface OnCreateReviewListener {

        void OnCreateReviewSuccess();

        void OnCreateReviewError();

    }

}