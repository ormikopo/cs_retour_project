package com.example.ormikopo.cs_retour_app.features.profile.data;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.profile.domain.UserDomain;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserProfileDetailsInteractor;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserRepository;

public class UserProfileDetailsInteractorImpl implements UserProfileDetailsInteractor, UserRepository.OnGetUserProfileDetailsFinishListener {

    private UserProfileDetailsInteractor.OnUserProfileDetailsFinishListener userProfileDetailsListener;
    private UserRepository userRepository;

    public UserProfileDetailsInteractorImpl(UserProfileDetailsInteractor.OnUserProfileDetailsFinishListener userProfileDetailsListener, Context context, boolean hardReloadDataFromNetwork) {
        this.userProfileDetailsListener = userProfileDetailsListener;
        this.userRepository = new UserRepositoryImpl(context, hardReloadDataFromNetwork);
    }

    @Override
    public void getUserProfileDetails(int userId) {
        this.userRepository.getUserProfileDetails(this, userId);
    }

    @Override
    public void OnGetUserProfileDetailsSuccess(UserEntity user) {

        UserDomain result = new UserDomain(
                String.valueOf(user.getId()),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getAddress(),
                user.getAge(),
                user.getTeam()
        );

        this.userProfileDetailsListener.OnSuccess(result);

    }

    @Override
    public void OnGetUserProfileDetailsError() {
        this.userProfileDetailsListener.OnError();
    }

}