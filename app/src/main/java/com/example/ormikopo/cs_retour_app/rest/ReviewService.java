package com.example.ormikopo.cs_retour_app.rest;

import com.example.ormikopo.cs_retour_app.features.reviews.data.ReviewEntity;
import com.example.ormikopo.cs_retour_app.rest.requests.CreateReviewRequest;
import com.example.ormikopo.cs_retour_app.utils.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ReviewService {

    @GET(Constants.RestApiEndpoints.REVIEWS)
    Call<ArrayList<ReviewEntity>> fetchTourPackageReviews(@Path("tourPackageId") String tourPackageId);

    @POST(Constants.RestApiEndpoints.REVIEWS)
    Call<Void> createTourPackageReview(@Path("tourPackageId") String tourPackageId, @Body CreateReviewRequest reviewDetails);

}