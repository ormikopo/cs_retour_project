package com.example.ormikopo.cs_retour_app.features.reviews.domain;

public interface ReviewsPresenter {

    void getReviews(String tourPackageId);

    void getFilteredReviews(String tourPackageId, String filter);

}