package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

import java.util.ArrayList;

public interface TourPackagesView {

    void showTourPackages(ArrayList<TourPackageUI> tourPackages);

    void showGeneralError();

}