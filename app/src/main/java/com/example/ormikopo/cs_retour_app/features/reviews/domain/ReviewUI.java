package com.example.ormikopo.cs_retour_app.features.reviews.domain;

public class ReviewUI {

    private String reviewerName;
    private String description;
    private int rating;
    private int ratingColor;

    public ReviewUI(String reviewerName, int rating, String description) {
        this.reviewerName = reviewerName;
        this.rating = rating;
        this.description = description;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRatingColor() {
        return ratingColor;
    }

    public void setRatingColor(int ratingColor) {
        this.ratingColor = ratingColor;
    }
}