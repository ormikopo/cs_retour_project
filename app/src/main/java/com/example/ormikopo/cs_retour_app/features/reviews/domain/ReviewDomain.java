package com.example.ormikopo.cs_retour_app.features.reviews.domain;

public class ReviewDomain {

    private String reviewerName;
    private int rating;
    private String description;

    public ReviewDomain(String reviewerName, int rating, String description) {
        this.reviewerName = reviewerName;
        this.rating = rating;
        this.description = description;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}