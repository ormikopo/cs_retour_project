package com.example.ormikopo.cs_retour_app.features.tourpackages.data;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageDomain;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageRegion;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesInteractor;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesRepository;

import java.util.ArrayList;

public class TourPackagesInteractorImpl implements TourPackagesInteractor, TourPackagesRepository.OnGetTourPackagesListener {

    private TourPackagesInteractor.OnTourPackagesFinishListener tourPackagesListener;
    private TourPackagesRepository tourPackagesRepository;

    public TourPackagesInteractorImpl(TourPackagesInteractor.OnTourPackagesFinishListener tourPackagesListener, Context context, boolean hardReloadDataFromNetwork) {
        this.tourPackagesListener = tourPackagesListener;
        this.tourPackagesRepository = new TourPackagesRepositoryImpl(context, hardReloadDataFromNetwork);
    }

    @Override
    public void getTourPackages() {
        this.tourPackagesRepository.getTourPackages(this);
    }

    @Override
    public void getFilteredTourPackages(TourPackageRegion regionEnum) {
        this.tourPackagesRepository.getFilteredTourPackages(regionEnum, this);
    }

    @Override
    public void OnGetTourPackagesSuccess(ArrayList<TourPackageEntity> tourPackages) {

        ArrayList<TourPackageDomain> result = new ArrayList<>();

        for (TourPackageEntity tourPackage : tourPackages) {

            result.add(
                    new TourPackageDomain(
                            tourPackage.getId(),
                            tourPackage.getName(),
                            tourPackage.getTotalRating(),
                            tourPackage.getRegion(),
                            tourPackage.getPhotoUrl()
                    )
            );

        }

        this.tourPackagesListener.OnSuccess(result);

    }

    @Override
    public void OnGetTourPackagesError() {
        this.tourPackagesListener.OnError();
    }

}