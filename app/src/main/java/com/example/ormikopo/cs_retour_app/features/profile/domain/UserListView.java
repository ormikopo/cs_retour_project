package com.example.ormikopo.cs_retour_app.features.profile.domain;

import java.util.ArrayList;

public interface UserListView {

    void showUsers(ArrayList<UserUI> users);

    void showGeneralError();

}