package com.example.ormikopo.cs_retour_app.features.reviews.domain;

import java.util.ArrayList;

public interface ReviewsInteractor {

    void getReviews(String tourPackageId);

    void getFilteredReviews(String tourPackageId, String filter);

    interface OnReviewsFinishListener{

        void OnSuccess(ArrayList<ReviewDomain> reviews);

        void OnError();

    }

}