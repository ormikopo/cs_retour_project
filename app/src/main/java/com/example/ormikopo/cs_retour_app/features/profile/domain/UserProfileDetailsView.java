package com.example.ormikopo.cs_retour_app.features.profile.domain;

public interface UserProfileDetailsView {

    void showUserProfileDetails(UserUI user);

    void showGeneralError();

}