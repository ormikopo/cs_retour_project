package com.example.ormikopo.cs_retour_app.features.profile.domain;

public interface UserProfileDetailsInteractor {

    void getUserProfileDetails(int userId);

    interface OnUserProfileDetailsFinishListener {

        void OnSuccess(UserDomain user);

        void OnError();

    }

}