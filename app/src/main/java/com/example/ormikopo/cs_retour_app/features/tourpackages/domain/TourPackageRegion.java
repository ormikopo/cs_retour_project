package com.example.ormikopo.cs_retour_app.features.tourpackages.domain;

import com.google.gson.annotations.SerializedName;

public enum TourPackageRegion
{
    ALL("All Regions"),

    @SerializedName("Crete")
    CRETE("Crete"),

    @SerializedName("Peloponnese")
    PELOPONNESE("Peloponnese"),

    @SerializedName("Macedonia")
    MACEDONIA("Macedonia"),

    @SerializedName("Thessaly")
    THESSALY("Thessaly"),

    @SerializedName("Thrace")
    THRACE("Thrace"),

    @SerializedName("Aeagean")
    AEAGEAN("Aeagean"),

    @SerializedName("Ionian")
    IONIAN("Ionian"),

    @SerializedName("StereaHellas")
    STEREAHELLAS("StereaHellas");

    private String region;

    TourPackageRegion(String region) {
        this.region = region;
    }

    public String getRegionStringValue() {
        return this.region;
    }

    @Override public String toString(){
        return region;
    }

}