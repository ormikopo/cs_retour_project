package com.example.ormikopo.cs_retour_app.base;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.login.presentation.LoginActivity;
import com.example.ormikopo.cs_retour_app.features.makereview.presentation.MakeReviewFragment;
import com.example.ormikopo.cs_retour_app.features.profile.presentation.ProfileScreenSliderFragment;
import com.example.ormikopo.cs_retour_app.features.reviews.presentation.ReviewsFragment;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;
import com.example.ormikopo.cs_retour_app.features.tourpackages.presentation.TourPackagesFragment;
import com.example.ormikopo.cs_retour_app.features.tours.presentation.ToursFragment;
import com.example.ormikopo.cs_retour_app.utils.Constants;
import com.example.ormikopo.cs_retour_app.utils.FragmentContainerHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.drawer_layout)
    transient DrawerLayout mDrawerLayout;

    @BindView(R.id.nav_view)
    transient NavigationView mNavigationView;

    @BindView(R.id.bottom_navigation)
    transient BottomNavigationView mBottomNavigationView;

    @BindView(R.id.toolbar)
    transient Toolbar mToolbar;

    @BindView(R.id.pull_to_refresh)
    transient SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check that the activity is using the main_content layout for rendering the fragments needed
        if (findViewById(R.id.main_content) != null) {

            // Setup Butterknife
            ButterKnife.bind(this);

            // Setup Toolbar
            setSupportActionBar(this.mToolbar);

            // Setup Drawer Layout
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, this.mDrawerLayout, this.mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

            this.mDrawerLayout.addDrawerListener(toggle);

            toggle.syncState();

            this.addNavigationItemSelectedListener(this);

            // Setup Bottom Navigation View
            this.mBottomNavigationView.getMenu().getItem(0).setChecked(false);
            this.mBottomNavigationView.getMenu().getItem(0).setCheckable(false);
            this.addBottomNavigationItemSelectedListener();

            // Setup Swipe to Refresh Layout Listener
            this.addSwipeLayoutOnRefreshListener();

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Programmatically add fragment to take place inside main_content layout of activity
            this.addTourPackagesFragment();

        }

    }

    @Override
    public void onBackPressed() {

        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();

        // Back stack has only the Main activity left without any fragment in it
        // We need to end the current activity and navigate the user to login again
        if (backStackEntryCount == 1 && this.isTaskRoot()) {

            new AlertDialog
                    .Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.back_stack_empty_message_title)
                    .setMessage(R.string.back_stack_empty_message_value)
                    .setPositiveButton(R.string.sign_out_prompt_positive, new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);

                            startActivity(intent);

                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            finish();

                        }

                    })
                    .setNegativeButton(R.string.sign_out_prompt_negative, null)
                    .show();

        }
        else {

            if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {

                this.mDrawerLayout.closeDrawer(GravityCompat.START);

            }
            else {

                super.onBackPressed();

            }

        }

    }

    @Override
    public void addTourPackagesFragment() {
        FragmentContainerHelper.replaceActivityMainContentWithFragment(this, TourPackagesFragment.newInstance(this), Constants.FragmentTags.TOUR_PACKAGES_FRAGMENT);
    }

    @Override
    public void addToursFragment(TourPackageUI tourPackage) {
        FragmentContainerHelper.replaceActivityMainContentWithFragment(this, ToursFragment.newInstance(this, tourPackage), Constants.FragmentTags.TOURS_FRAGMENT);
    }

    @Override
    public void addReviewsFragment(TourPackageUI tourPackage) {
        FragmentContainerHelper.replaceActivityMainContentWithFragment(this, ReviewsFragment.newInstance(this, tourPackage), Constants.FragmentTags.REVIEWS_FRAGMENT);
    }

    @Override
    public void addMakeReviewFragment(TourPackageUI tourPackage) {
        FragmentContainerHelper.replaceActivityMainContentWithFragment(this, MakeReviewFragment.newInstance(tourPackage), Constants.FragmentTags.MAKE_REVIEW_FRAGMENT);
    }

    @Override
    public void addProfileFragment() {
        FragmentContainerHelper.replaceActivityMainContentWithFragment(this, ProfileScreenSliderFragment.newInstance(), Constants.FragmentTags.PROFILE_FRAGMENT);
    }

    private void addNavigationItemSelectedListener(final FragmentActivity currentActivity) {

        this.mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        // Handle navigation view item clicks here.
                        int id = menuItem.getItemId();

                        // Add code here to update the UI based on the item selected
                        if(id == R.id.nav_profile) {

                            ProfileScreenSliderFragment profileFragment = (ProfileScreenSliderFragment)getSupportFragmentManager()
                                    .findFragmentByTag(Constants.FragmentTags.PROFILE_FRAGMENT);

                            if (profileFragment == null || (profileFragment != null && !profileFragment.isVisible())) {
                                addProfileFragment();
                            }

                        }
                        else if (id == R.id.nav_sign_out) {

                            Intent intent = new Intent(currentActivity, LoginActivity.class);

                            currentActivity.startActivity(intent);

                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            // for ending the current activity that this fragment is hooked on
                            currentActivity.finish();

                        }

                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawer(GravityCompat.START);

                        return true;

                    }
                }
        );

    }

    private void addBottomNavigationItemSelectedListener() {

        this.mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                menuItem.setCheckable(false);
                menuItem.setChecked(false);

                switch (menuItem.getItemId()) {

                    case R.id.navigation_home:

                        TourPackagesFragment tourPackagesFragment = (TourPackagesFragment)getSupportFragmentManager()
                                .findFragmentByTag(Constants.FragmentTags.TOUR_PACKAGES_FRAGMENT);

                        if (tourPackagesFragment == null || (tourPackagesFragment != null && !tourPackagesFragment.isVisible())) {
                            addTourPackagesFragment();
                        }

                        return true;

                    case R.id.menu_refresh:

                        // Signal SwipeRefreshLayout to start the progress indicator
                        mSwipeRefreshLayout.setRefreshing(true);

                        // Start the refresh background task.
                        // This calls setRefreshing(false) when it finishes
                        onUserRefresh();

                        return true;

                    case R.id.navigation_back:

                        onBackPressed();

                        return true;

                }

                return false;

            }

        });

    }

    private void addSwipeLayoutOnRefreshListener() {

        this.mSwipeRefreshLayout.setOnRefreshListener(

                new SwipeRefreshLayout.OnRefreshListener() {

                    @Override
                    public void onRefresh() {

                        // This method performs the actual data-refresh operation.
                        onUserRefresh();

                    }

                }

        );

    }

    private void onUserRefresh() {

        // Check which fragment is active when the refresh button got clicked
        // and reload it
        TourPackagesFragment tourPackagesFragment = (TourPackagesFragment)getSupportFragmentManager()
                .findFragmentByTag(Constants.FragmentTags.TOUR_PACKAGES_FRAGMENT);

        // Tour Packages Fragment
        if (tourPackagesFragment != null && tourPackagesFragment.isVisible()) {

            reloadCurrentFragment(tourPackagesFragment);

            mSwipeRefreshLayout.setRefreshing(false);

            return;

        }

        // Tours Fragment
        ToursFragment toursFragment = (ToursFragment)getSupportFragmentManager()
                .findFragmentByTag(Constants.FragmentTags.TOURS_FRAGMENT);

        if (toursFragment != null && toursFragment.isVisible()) {

            reloadCurrentFragment(toursFragment);

            mSwipeRefreshLayout.setRefreshing(false);

            return;

        }

        // Reviews Fragment
        ReviewsFragment reviewsFragment = (ReviewsFragment)getSupportFragmentManager()
                .findFragmentByTag(Constants.FragmentTags.REVIEWS_FRAGMENT);

        if (reviewsFragment != null && reviewsFragment.isVisible()) {

            reloadCurrentFragment(reviewsFragment);

            mSwipeRefreshLayout.setRefreshing(false);

            return;

        }

        // Profile Fragment
        ProfileScreenSliderFragment profileFragment = (ProfileScreenSliderFragment)getSupportFragmentManager()
                .findFragmentByTag(Constants.FragmentTags.PROFILE_FRAGMENT);

        if (profileFragment != null && profileFragment.isVisible()) {

            reloadCurrentFragment(profileFragment);

            mSwipeRefreshLayout.setRefreshing(false);

            return;

        }

        // For other fragments (e.g. Make Review Fragment) there will be no server data we need to load - Just return
        mSwipeRefreshLayout.setRefreshing(false);

        return;

    }

    private void reloadCurrentFragment(Fragment frg) {

        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        Bundle args = (frg.getArguments() != null) ? frg.getArguments() : new Bundle();

        args.putBoolean(Constants.BundleKeys.HARD_RELOAD, true);

        frg.setArguments(args);

        ft.detach(frg);
        ft.attach(frg);

        ft.commit();

    }

    private Fragment getVisibleFragment() {

        List<Fragment> fragments = getSupportFragmentManager().getFragments();

        for (Fragment fragment : fragments) {

            if (fragment != null && fragment.isVisible()) {

                return fragment;

            }

        }

        return null;

    }

}