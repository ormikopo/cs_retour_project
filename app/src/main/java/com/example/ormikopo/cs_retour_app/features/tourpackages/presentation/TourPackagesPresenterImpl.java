package com.example.ormikopo.cs_retour_app.features.tourpackages.presentation;

import android.content.Context;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.tourpackages.data.TourPackagesInteractorImpl;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageDomain;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageRegion;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackageUI;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesInteractor;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesPresenter;
import com.example.ormikopo.cs_retour_app.features.tourpackages.domain.TourPackagesView;

import java.util.ArrayList;

public class TourPackagesPresenterImpl implements TourPackagesPresenter, TourPackagesInteractor.OnTourPackagesFinishListener {

    private TourPackagesView tourPackagesView;
    private TourPackagesInteractor interactor;

    public TourPackagesPresenterImpl(TourPackagesView tourPackagesView, Context context, boolean hardReloadDataFromNetwork) {
        this.tourPackagesView = tourPackagesView;
        this.interactor = new TourPackagesInteractorImpl(this, context, hardReloadDataFromNetwork);
    }

    @Override
    public void getTourPackages() {
        this.interactor.getTourPackages();
    }

    @Override
    public void getFilteredTourPackages(TourPackageRegion filterByRegion) {
        this.interactor.getFilteredTourPackages(filterByRegion);
    }

    @Override
    public void OnSuccess(ArrayList<TourPackageDomain> tourPackages) {

        ArrayList<TourPackageUI> tourPackagesUI = new ArrayList<>();

        if (tourPackages != null && !tourPackages.isEmpty()) {

            for (TourPackageDomain tourPackage : tourPackages) {

                TourPackageUI tourPackageUI = new TourPackageUI(
                        tourPackage.getPhotoUrl(),
                        tourPackage.getId(),
                        tourPackage.getName(),
                        tourPackage.getRating(),
                        tourPackage.getRegion()
                );

                float packageRating = tourPackage.getRating();

                if (packageRating > 0 && packageRating < 3) {
                    tourPackageUI.setRatingColor(R.color.colorRed);
                }
                else if (packageRating == 3) {
                    tourPackageUI.setRatingColor(R.color.colorYellow);
                }
                else if(packageRating > 3 && packageRating <= 5){
                    tourPackageUI.setRatingColor(R.color.colorGreen);
                }
                else {
                    tourPackageUI.setRatingColor(R.color.colorPrimary);
                }

                switch (tourPackageUI.getRegion()){
                    case CRETE:
                        tourPackageUI.setRegionColor(R.color.colorAccent);
                        break;
                    case IONIAN:
                        tourPackageUI.setRegionColor(R.color.colorBlack);
                        break;
                    case THRACE:
                        tourPackageUI.setRegionColor(R.color.colorPrimaryDark);
                        break;
                    case AEAGEAN:
                        tourPackageUI.setRegionColor(R.color.colorBlue);
                        break;
                    case THESSALY:
                        tourPackageUI.setRegionColor(R.color.colorOrange);
                        break;
                    case MACEDONIA:
                        tourPackageUI.setRegionColor(R.color.colorGreen);
                        break;
                    case PELOPONNESE:
                        tourPackageUI.setRegionColor(R.color.colorPurple);
                        break;
                    case STEREAHELLAS:
                        tourPackageUI.setRegionColor(R.color.colorRed);
                        break;
                     default:
                         tourPackageUI.setRegionColor(R.color.colorPrimary);
                }

                tourPackagesUI.add(tourPackageUI);

            }

        }

        this.tourPackagesView.showTourPackages(tourPackagesUI);
    }

    @Override
    public void OnError() {
        this.tourPackagesView.showGeneralError();
    }

}