package com.example.ormikopo.cs_retour_app.features.tours.presentation;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.tours.domain.OnTourClickListener;
import com.example.ormikopo.cs_retour_app.features.tours.domain.TourUI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ToursRvAdapter extends RecyclerView.Adapter<ToursRvAdapter.ToursViewHolder> {

    private ArrayList<TourUI> tours;
    private OnTourClickListener listener;

    public ToursRvAdapter(ArrayList<TourUI> tours, OnTourClickListener listener) {
        this.tours = tours;
        this.listener = listener;
    }

    public static class ToursViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tour_logo)
        ImageView mTourLogo;

        @BindView(R.id.tour_name)
        TextView mTourName;

        @BindView(R.id.tour_description)
        TextView mTourDescription;

        @BindView(R.id.tour_item_root)
        LinearLayout mTourItemRoot;

        public ToursViewHolder(View v) {

            super(v);

            ButterKnife.bind(this, v);

        }
    }

    @NonNull
    @Override
    public ToursViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.view_tour_item, viewGroup, false);

        ToursViewHolder vh = new ToursViewHolder(view);

        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull ToursViewHolder holder, int position) {

        final TourUI tour = this.tours.get(position);

        Picasso.get()
            .load(tour.getPhotoUrl())
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(holder.mTourLogo);

        holder.mTourName.setText(tour.getName());

        holder.mTourDescription.setTextSize(tour.getDescTextSize());
        holder.mTourDescription.setText(tour.getDescription());

        holder.mTourItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTourClicked(tour);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.tours.size();
    }

}