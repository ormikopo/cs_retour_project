package com.example.ormikopo.cs_retour_app.features.profile.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.features.profile.domain.OnUserClickListener;
import com.example.ormikopo.cs_retour_app.features.profile.domain.UserUI;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersRvAdapter extends RecyclerView.Adapter<UsersRvAdapter.UsersViewHolder> {

    private ArrayList<UserUI> users;
    private OnUserClickListener listener;
    private Context context;

    public UsersRvAdapter(ArrayList<UserUI> users, OnUserClickListener listener, Context context) {
        this.users = users;
        this.listener = listener;
        this.context = context;
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_logo)
        View mUserLogo;

        @BindView(R.id.user_username)
        TextView mUsername;

        @BindView(R.id.user_full_name)
        TextView mUserFullName;

        @BindView(R.id.user_team)
        TextView mUserTeam;

        @BindView(R.id.user_list_item_root)
        RelativeLayout mUserListItemRoot;

        public UsersViewHolder(View v) {

            super(v);

            ButterKnife.bind(this, v);

        }
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {

        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.view_user_list_item, viewGroup, false);

        UsersViewHolder vh = new UsersViewHolder(view);

        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder holder, int position) {

        final UserUI user = this.users.get(position);

        holder.mUserLogo.setBackgroundColor(context.getResources().getColor(user.getTeamColor()));

        holder.mUsername.setText(user.getUsername());
        holder.mUserFullName.setText(user.getFirstName() + " " + user.getLastName());
        holder.mUserTeam.setText("Team " + user.getTeam());

        holder.mUserListItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onUserClicked(user);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

}