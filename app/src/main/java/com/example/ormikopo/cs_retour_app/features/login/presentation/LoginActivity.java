package com.example.ormikopo.cs_retour_app.features.login.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ormikopo.cs_retour_app.R;
import com.example.ormikopo.cs_retour_app.utils.Constants;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Programmatically add fragment to take place inside activity
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.login_root, LoginFragment.newInstance(), Constants.FragmentTags.LOGIN_FRAGMENT)
                .commit();

    }

}