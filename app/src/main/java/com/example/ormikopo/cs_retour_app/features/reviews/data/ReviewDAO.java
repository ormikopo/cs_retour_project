package com.example.ormikopo.cs_retour_app.features.reviews.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.example.ormikopo.cs_retour_app.base.GenericDAO;

import java.util.List;

@Dao
public abstract class ReviewDAO extends GenericDAO<ReviewEntity> {

    @Query("SELECT * FROM reviews WHERE tour_package_id = :tourPackageId")
    public abstract List<ReviewEntity> getReviewsOfTourPackage(String tourPackageId);

    @Transaction
    public void updateReviewsOfTourPackage(String tourPackageId, ReviewEntity... reviewEntities) {
        deleteReviewsOfTourPackage(tourPackageId);
        insertMany(reviewEntities);
    }

    @Transaction
    public void updateReviewOfUserForTourPackage(String username, String tourPackageId, ReviewEntity reviewEntity) {
        deleteReviewOfUserForTourPackage(username, tourPackageId);
        insert(reviewEntity);
    }

    @Query("DELETE FROM reviews WHERE tour_package_id = :tourPackageId")
    public abstract void deleteReviewsOfTourPackage(String tourPackageId);

    @Query("DELETE FROM reviews WHERE username = :username AND tour_package_id = :tourPackageId")
    public abstract void deleteReviewOfUserForTourPackage(String username, String tourPackageId);

}